#include "move_control.h"
#include "line_track.h"
#include "main.h"
#include "math.h"
#include "oled.h"
#include "stdio.h"
#include "stdlib.h"
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"
#include "bsp_can.h"
#include "tim.h"

#define Anlog_Digtal 1500


extern All_car_Typedef car;
extern uint8_t color_save[3];
extern uint8_t position[3];
extern uint8_t Color;
extern uint8_t Color_c;

void Move_Left(uint8_t cell)
{
	switch(cell)
		{
			case 0:  // 向左移动一格
				TraceL_Turning_Laps(20, 2000);
				Direction_Initial(Left);
				while(!DU1)
					{
						TraceL_speed(1500);
						vTaskDelay(2);
					}
					Stop();
				break;
			case 1:  // 中间灰度移动到中线
				TraceL_Turning_Laps(15, 2000);
				TraceL_Turning_Laps(20, 2000);
				Direction_Initial(Left);
				while(ADC_Value[1] < Anlog_Digtal)
					{
						TraceL_speed(2000);
						vTaskDelay(2);
					}
					Stop();
				break;
			case 2:		// 左侧灰度移动到最左边的线
				TraceL_Turning_Laps(25, 1500);
				TraceL_Turning_Laps(50, 2500);
				TraceL_Turning_Laps(10, 1500);
				Direction_Initial(Left);
				while(!DU1)
					TraceL_speed(2000);
					vTaskDelay(2);
				Stop();
				break;
			default:
				break;
		}
}

void Move_Right(uint8_t cell)
{
	switch(cell)
			{
				case 0:  // 向右移动一格
					TraceR_Turning_Laps(20, 2000);
					Direction_Initial(Right);
					while(!DU2)
						{
							TraceR_speed(2000);
							vTaskDelay(2);
						}
					Stop();
					break;
				case 1:  // 中间灰度移动到中线
//					TraceR_Turning_Laps(20, 2000);
					Direction_Initial(Right);
					while(ADC_Value[0] < Anlog_Digtal)
						{
							TraceR_speed(2000);
							vTaskDelay(2);
						}
						Stop();
					break;
				case 2:		// 左侧灰度移动到最右边的线
					TraceR_Turning_Laps(25, 1500);
					TraceR_Turning_Laps(50, 2500);
					TraceR_Turning_Laps(10, 1500);
					Direction_Initial(Right);
					while(!DU2)
						TraceR_speed(2000);
						vTaskDelay(2);
					Stop();
					break;
				default:
					break;
			}
}

void Move_Forward(void)
{
	Direction_Initial(Forward);
	car.speed_L = 1500;
	car.speed_R = 1500;
	while(ADC_Value[14] < Anlog_Digtal)
		vTaskDelay(2);
	Stop();
}

void Move_Forward_Middle(void)
{
	Direction_Initial(Forward);
	car.speed_L = 1500;
	car.speed_R = 1500;
	while(ADC_Value[14] < Anlog_Digtal)
		vTaskDelay(2);
	Stop();
}

void Move_Forward_Put_Close(void)
{
	Turning_Laps(20, 800,Forward);
}

void Move_Forward_Put_Middle_Close(void)
{
	TraceF_Turning_Laps(20, 800);
}


// 往左线巡Direction设置为0，往右设置为1
void Move_Back(uint8_t Direction)
{
	Turning_Laps(30,2000,Back);
	Direction_Initial(Back);
	car.speed_L = 1500;
	car.speed_R = 1500;
	if(!Direction)
		{
			while(ADC_Value[12] < Anlog_Digtal)
				vTaskDelay(2);
		}
	else
		{
			while(ADC_Value[4] < Anlog_Digtal)
				vTaskDelay(2);
		}
	Stop();
}

void Left_Aligment(void)
{
	Direction_Initial(Left);
	car.speed_L = 1500;
	car.speed_R = 1500;
	while(!DU1)
		vTaskDelay(2);
	Stop();
}

void Right_Aligment(void)
{
	Direction_Initial(Right);
	car.speed_L = 1500;
	car.speed_R = 1500;
	while(!DU2)
		vTaskDelay(2);
	Stop();
}

void Trace_Left_Aligment(void)
{
	Direction_Initial(Left);
	while(!DU1)
		{
			TraceL_speed(1000);
			vTaskDelay(2);
		}
	Stop();
}

void Trace_Right_Aligment(void)
{
	Direction_Initial(Right);
	while(!DU2)
		{
			TraceR_speed(1000);
			vTaskDelay(2);
		}
	Stop();
}

void Algment(uint8_t times)
{
	uint8_t tmp = 0;
	while(tmp<times)
		{
			Trace_Left_Aligment();
			Trace_Right_Aligment();
			tmp++;
		}
		Stop();
}

void Move_Foreward_Close(void)
{
	Turning_Laps(40, 800, Forward);
}

void Circle_Saver(uint8_t position, int speed)
{
	switch(position)
		{
			case 0:
				Servo_Move(TIM_CHANNEL_4,77,speed);
				break;
			case 1:
				Servo_Move(TIM_CHANNEL_4,122,speed);
				break;
			case 2:
				Servo_Move(TIM_CHANNEL_4,167,speed);
				break;
			case 3:
				Servo_Move(TIM_CHANNEL_4,214,speed);
				break;
			case 4:
				Servo_Move(TIM_CHANNEL_4,260,speed);
				break;
			default:
				break;
		}
}

void Open_Catcher(void)
{
	Servo_Move(TIM_CHANNEL_1,150,3);
}

void Close_Catcher(void)
{
	Servo_Move(TIM_CHANNEL_1,60,3);
}

void Put_Block(void)
{
	Servo_Move(TIM_CHANNEL_3,286,3);
}

void Get_Block(void)
{
	Servo_Move(TIM_CHANNEL_3,60,2);
}

void Stand_Block(void)
{
	Servo_Move(TIM_CHANNEL_3,182,2);
}

void Highter_Top(void)
{
	Servo_Move(TIM_CHANNEL_2,289,2);
}

void Highter_Middle(void)
{
	Servo_Move(TIM_CHANNEL_2,86,2);
}

void Highter_Buttop(void)
{
	Servo_Move(TIM_CHANNEL_2,55,4);
}

void Highter_Palletizing(void)
{
	Servo_Move(TIM_CHANNEL_2,162,2);
}

void Highter_Buttonest(void)
{
	Servo_Move(TIM_CHANNEL_3,55,2);
}

uint8_t Catch_First(void)
{
	Move_Left(0);
	Circle_Saver(0,0);
	Highter_Top();
	Get_Block();
	Open_Catcher();
	Move_Forward();
	Move_Foreward_Close();
	Stop();
	Close_Catcher();
	vTaskDelay(500);
	color_save[0] = Color_c;
	if(UART2_RECV_BUF[0] == color_save[0] + '0') // 第一个颜色正确取走，退出
		{
			Right_Aligment();
			position[0] = 0;
			Move_Back(0);
			Put_Block();
			vTaskDelay(500);
			Open_Catcher();
			Circle_Saver(1, 1);
			Get_Block();
			Circle_Saver(2, 0);
			return 1;
		}
	else																		// 前往第二个物块夹取区域
		{
			Open_Catcher();
			Turning_Laps(50,800,Back);
			Right_Aligment();
			Move_Back(0);
			Move_Left(1);
			Move_Forward_Middle();
			Move_Foreward_Close();
			Stop();
			Close_Catcher();
			vTaskDelay(500);
			color_save[1] = Color_c;
			if(UART2_RECV_BUF[0] == color_save[1] + '0') // 第一个颜色正确取走，退出
				{
					position[0] = 1;
					Move_Back(0);
					Put_Block();
					vTaskDelay(500);
					Open_Catcher();
					Circle_Saver(1, 1);
					Get_Block();
					Circle_Saver(2, 0);
					return 2;
				}
			else
				{
					color_save[2] = 6 - color_save[0] - color_save[1];
					Open_Catcher();
					Turning_Laps(50,800,Back);
					Move_Back(0);
					Move_Left(0);
					Move_Forward();
					Move_Foreward_Close();
					Close_Catcher();
					vTaskDelay(500);
					Left_Aligment();
					position[0] = 2;
					Move_Back(1);
					Put_Block();
					vTaskDelay(500);
					Open_Catcher();
					Circle_Saver(1, 1);
					Get_Block();
					Circle_Saver(2, 0);
					return 3;
				}
		}
}

uint8_t Catch_Second(uint8_t Position)
{
	switch(Position)
		{
			case 1:			
				Open_Catcher();
				Move_Left(1);
				Move_Forward_Middle();
				Move_Foreward_Close();
				Stop();
				Close_Catcher();
				vTaskDelay(500);
				color_save[1] = Color_c;
				if(UART2_RECV_BUF[1] == color_save[1] + '0') // 第一个颜色正确取走，退出
					{
						position[1] = 1;
						Move_Back(0);
						Put_Block();
						vTaskDelay(500);
						Open_Catcher();
						Circle_Saver(3, 1);
						Get_Block();
						Circle_Saver(4, 0);
						return 2;
					}
				else
					{
						color_save[2] = 6 - color_save[0] - color_save[1];
						Open_Catcher();
						Turning_Laps(50,800,Back);
						Move_Back(0);
						Move_Left(0);
						Move_Forward();
						Move_Foreward_Close();
						Close_Catcher();
						vTaskDelay(500);
						Left_Aligment();
						position[1] = 2;
						Move_Back(1);
						Put_Block();
						vTaskDelay(500);
						Open_Catcher();
						Circle_Saver(3, 1);
						Get_Block();
						Circle_Saver(4, 0);
						return 3;
					}
			case 2:
				if(UART2_RECV_BUF[1] == color_save[0] + '0') // 抓取的位置在第一个位置
					{
						color_save[2] = 6 - color_save[0] - color_save[1];
						Move_Right(0);
						Move_Forward();
						Move_Foreward_Close();
						Close_Catcher();
						vTaskDelay(500);
						Right_Aligment();
						position[1] = 2;
						Move_Back(0);
						Put_Block();
						vTaskDelay(500);
						Open_Catcher();
						Circle_Saver(3, 1);
						Get_Block();
						Circle_Saver(4, 0);
						return 1;
					}
				else 	// 抓取的位置在第三个位置
					{
						color_save[2] = 6 - color_save[0] - color_save[1];
						Open_Catcher();
						Move_Left(0);
						Move_Forward();
						Move_Foreward_Close();
						Close_Catcher();
						vTaskDelay(500);
						Left_Aligment();
						position[1] = 2;
						Move_Back(1);
						Put_Block();
						vTaskDelay(500);
						Open_Catcher();
						Circle_Saver(3, 1);
						Get_Block();
						Circle_Saver(4, 0);
						return 3;
					}
			case 3:
				if(UART2_RECV_BUF[1] == color_save[0] + '0') // 抓取的物块在第一个位置
					{
						Move_Right(2);
						Move_Forward();
						Move_Foreward_Close();
						Close_Catcher();
						vTaskDelay(500);
						Left_Aligment();
						position[1] = 1;
						Move_Back(0);
						Put_Block();
						vTaskDelay(500);
						Open_Catcher();
						Circle_Saver(3, 1);
						Get_Block();
						Circle_Saver(4, 0);
						return 1;
					}
				else
					{
						Open_Catcher();
						Move_Right(1);
						Move_Forward_Middle();
						Move_Foreward_Close();
						Stop();
						Close_Catcher();
						vTaskDelay(500);
						color_save[1] = Color_c;
						position[1] = 2;
						Move_Back(0);
						Put_Block();
						vTaskDelay(500);
						Open_Catcher();
						Circle_Saver(3, 1);
						Get_Block();
						Circle_Saver(4, 0);
						return 2;
					}
			default:
				break;
		}
	return 0;
}

uint8_t Catch_Third(uint8_t Position)
{
	switch(Position)
			{
				case 1:
					if(UART2_RECV_BUF[2] == color_save[1] + '0') // 目标放置在第二个位置
						{
							Move_Left(1);
							Move_Forward_Middle();
							Move_Foreward_Close();
							Stop();
							Close_Catcher();
							vTaskDelay(500);
							Move_Back(0);
							Put_Block();
							vTaskDelay(500);
							Open_Catcher();
							Circle_Saver(0, 0);
							Move_Right(0);
							Move_Right(1);
							return 2;
						}
					else
						{
							Move_Left(2);
							Open_Catcher();
							Move_Forward();
							Move_Foreward_Close();
							Close_Catcher();
							vTaskDelay(500);
							Left_Aligment();
							Move_Back(1);
							Put_Block();
							vTaskDelay(500);
							Open_Catcher();
							Circle_Saver(0, 0);
							Move_Right(2);
							Move_Right(1);
							return 3;
						}
				case 2:
					if(UART2_RECV_BUF[2] == color_save[0] + '0') // 抓取的位置在第一个位置
						{
							Move_Right(0);
							Move_Forward();
							Move_Foreward_Close();
							Close_Catcher();
							vTaskDelay(500);
							Right_Aligment();
							Move_Back(0);
							Put_Block();
							vTaskDelay(500);
							Open_Catcher();
							Circle_Saver(0, 0);
							Move_Right(1);
							return 1;
						}
					else 	// 抓取的位置在第三个位置
						{
							color_save[2] = 6 - color_save[0] - color_save[1];
							Open_Catcher();
							Move_Left(0);
							Move_Forward();
							Move_Foreward_Close();
							Close_Catcher();
							vTaskDelay(500);
							Left_Aligment();
							Move_Back(1);
							Put_Block();
							vTaskDelay(500);
							Open_Catcher();
							Circle_Saver(0, 0);
							Move_Right(2);
							Move_Right(1);
							return 3;
						}
				case 3:
					if(UART2_RECV_BUF[2] == color_save[1] + '0')
						{
							Move_Right(1);
							Move_Forward_Middle();
							Move_Foreward_Close();
							Stop();
							Close_Catcher();
							vTaskDelay(500);
							position[0] = 1;
							Move_Back(0);
							Put_Block();
							vTaskDelay(500);
							Open_Catcher();
							Circle_Saver(0, 0);
							Move_Right(0);
							Move_Right(1);
							return 2;
						}
					else
						{
							Move_Right(2);
							Move_Forward();
							Move_Foreward_Close();
							Close_Catcher();
							vTaskDelay(500);
							Right_Aligment();
							Move_Back(0);
							Put_Block();
							vTaskDelay(500);
							Open_Catcher();
							Circle_Saver(0, 0);
							Move_Right(1);
							return 1;
						}
				default:
					break;
			}
	return 0;
}

uint8_t Catch_Second_First(void)
{
	color_save[0] = 0;
	color_save[1] = 0;
	color_save[2] = 0;
	Move_Left(0);
	Circle_Saver(0,0);
	Highter_Middle();
	Get_Block();
	Open_Catcher();
	Move_Forward();
	Move_Foreward_Close();
	Stop();
	Close_Catcher();
	vTaskDelay(500);
	color_save[0] = Color_c;
	if(UART2_RECV_BUF[4] == color_save[0] + '0') // 第一个颜色正确取走，退出
		{
			Right_Aligment();
			position[0] = 0;
			Move_Back(0);
			Highter_Top();
			Put_Block();
			vTaskDelay(500);
			Open_Catcher();
			Circle_Saver(1, 1);
			Get_Block();
			Highter_Middle();
			Circle_Saver(2, 0);
			return 1;
		}
	else																		// 前往第二个物块夹取区域
		{
			Open_Catcher();
			Turning_Laps(50,800,Back);
			Right_Aligment();
			Move_Back(0);
			Move_Left(1);
			Move_Forward_Middle();
			Move_Foreward_Close();
			Stop();
			Close_Catcher();
			vTaskDelay(500);
			color_save[1] = Color_c;
			if(UART2_RECV_BUF[4] == color_save[1] + '0') // 第一个颜色正确取走，退出
				{
					position[0] = 1;
					Move_Back(0);
					Highter_Top();
					Put_Block();
					vTaskDelay(500);
					Open_Catcher();
					Circle_Saver(1, 1);
					Get_Block();
					Highter_Middle();
					Circle_Saver(2, 0);
					return 2;
				}
			else
				{
					color_save[2] = 6 - color_save[0] - color_save[1];
					Open_Catcher();
					Turning_Laps(50,800,Back);
					Move_Back(0);
					Move_Left(0);
					Move_Forward();
					Move_Foreward_Close();
					Close_Catcher();
					vTaskDelay(500);
					Left_Aligment();
					position[0] = 2;
					Move_Back(1);
					Highter_Top();
					Put_Block();
					vTaskDelay(500);
					Open_Catcher();
					Circle_Saver(1, 1);
					Get_Block();
					Highter_Middle();
					Circle_Saver(2, 0);
					return 3;
				}
		}
}

uint8_t Catch_Second_Second(uint8_t Position)
{
	switch(Position)
		{
			case 1:			
				Open_Catcher();
				Move_Left(1);
				Move_Forward_Middle();
				Move_Foreward_Close();
				Stop();
				Close_Catcher();
				vTaskDelay(500);
				color_save[1] = Color_c;
				if(UART2_RECV_BUF[5] == color_save[1] + '0') // 第一个颜色正确取走，退出
					{
						position[1] = 1;
						Move_Back(0);
						Highter_Top();
						Put_Block();
						vTaskDelay(500);
						Open_Catcher();
						Circle_Saver(3, 1);
						Get_Block();
						Highter_Middle();
						Circle_Saver(4, 0);
						return 2;
					}
				else
					{
						color_save[2] = 6 - color_save[0] - color_save[1];
						Open_Catcher();
						Turning_Laps(50,800,Back);
						Move_Back(0);
						Move_Left(0);
						Move_Forward();
						Move_Foreward_Close();
						Close_Catcher();
						vTaskDelay(500);
						Left_Aligment();
						position[1] = 2;
						Move_Back(1);
						Highter_Top();
						Put_Block();
						vTaskDelay(500);
						Open_Catcher();
						Circle_Saver(3, 1);
						Get_Block();
						Highter_Middle();
						Circle_Saver(4, 0);
						return 3;
					}
			case 2:
				if(UART2_RECV_BUF[5] == color_save[0] + '0') // 抓取的位置在第一个位置
					{
						color_save[2] = 6 - color_save[0] - color_save[1];
						Move_Right(0);
						Move_Forward();
						Move_Foreward_Close();
						Close_Catcher();
						vTaskDelay(500);
						Left_Aligment();
						position[1] = 0;
						Move_Back(0);
						Highter_Top();
						Put_Block();
						vTaskDelay(500);
						Open_Catcher();
						Circle_Saver(3, 1);
						Get_Block();
						Highter_Middle();
						Circle_Saver(4, 0);
						return 1;
					}
				else 	// 抓取的位置在第三个位置
					{
						color_save[2] = 6 - color_save[0] - color_save[1];
						Open_Catcher();
						Move_Left(0);
						Move_Forward();
						Move_Foreward_Close();
						Close_Catcher();
						vTaskDelay(500);
						Left_Aligment();
						position[1] = 2;
						Move_Back(1);
						Highter_Top();
						Put_Block();
						vTaskDelay(500);
						Open_Catcher();
						Circle_Saver(3, 1);
						Get_Block();
						Highter_Middle();
						Circle_Saver(4, 0);
						return 3;
					}
			case 3:
				if(UART2_RECV_BUF[5] == color_save[0] + '0') // 抓取的物块在第一个位置
					{
						Move_Right(2);
						Move_Forward();
						Move_Foreward_Close();
						Close_Catcher();
						vTaskDelay(500);
						Right_Aligment();
						position[1] = 0;
						Move_Back(0);
						Highter_Top();
						Put_Block();
						vTaskDelay(500);
						Open_Catcher();
						Circle_Saver(3, 1);
						Get_Block();
						Highter_Middle();
						Circle_Saver(4, 0);
						return 1;
					}
				else
					{
						Open_Catcher();
						Move_Right(1);
						Move_Forward_Middle();
						Move_Foreward_Close();
						Stop();
						Close_Catcher();
						vTaskDelay(500);
						color_save[1] = Color_c;
						position[1] = 1;
						Move_Back(0);
						Highter_Top();
						Put_Block();
						vTaskDelay(500);
						Open_Catcher();
						Circle_Saver(3, 1);
						Get_Block();
						Highter_Middle();
						Circle_Saver(4, 0);
						return 2;
					}
			default:
				break;
		}
	return 0;
}

uint8_t Catch_Second_Third(uint8_t Position)
{
	switch(Position)
			{
				case 1:
					if(UART2_RECV_BUF[6] == color_save[1] + '0') // 目标放置在第二个位置
						{
							Move_Left(1);
							Move_Forward_Middle();
							Move_Foreward_Close();
							Stop();
							Close_Catcher();
							vTaskDelay(500);
							Move_Back(0);
							Highter_Top();
							Put_Block();
							vTaskDelay(500);
							Open_Catcher();
							Circle_Saver(0, 0);
							Move_Right(0);
							Move_Right(1);
							return 2;
						}
					else
						{
							Move_Left(2);
							Open_Catcher();
							Move_Forward();
							Move_Foreward_Close();
							Close_Catcher();
							vTaskDelay(500);
							Left_Aligment();
							Move_Back(1);
							Highter_Top();
							Put_Block();
							vTaskDelay(500);
							Open_Catcher();
							Circle_Saver(0, 0);
							Move_Right(2);
							Move_Right(1);
							return 3;
						}
				case 2:
					if(UART2_RECV_BUF[6] == color_save[0] + '0') // 抓取的位置在第一个位置
						{
							Move_Right(0);
							Move_Forward();
							Move_Foreward_Close();
							Close_Catcher();
							vTaskDelay(500);
							Right_Aligment();
							Move_Back(0);
							Highter_Top();
							Put_Block();
							vTaskDelay(500);
							Open_Catcher();
							Circle_Saver(0, 0);
							Move_Right(1);
							return 1;
						}
					else 	// 抓取的位置在第三个位置
						{
							color_save[2] = 6 - color_save[0] - color_save[1];
							Open_Catcher();
							Move_Left(0);
							Move_Forward();
							Move_Foreward_Close();
							Close_Catcher();
							vTaskDelay(500);
							Left_Aligment();
							Move_Back(1);
							Highter_Top();
							Put_Block();
							vTaskDelay(500);
							Open_Catcher();
							Circle_Saver(0, 0);
							Move_Right(2);
							Move_Right(1);
							return 3;
						}
				case 3:
					if(UART2_RECV_BUF[6] == color_save[1] + '0')
						{
							Move_Right(1);
							Move_Forward_Middle();
							Move_Foreward_Close();
							Stop();
							Close_Catcher();
							vTaskDelay(500);
							position[0] = 1;
							Move_Back(0);
							Highter_Top();
							Put_Block();
							vTaskDelay(500);
							Open_Catcher();
							Circle_Saver(0, 0);
							Move_Right(0);
							Move_Right(1);
							return 2;
						}
					else
						{
							Move_Right(2);
							Move_Forward();
							Move_Foreward_Close();
							Close_Catcher();
							vTaskDelay(500);
							Right_Aligment();
							Move_Back(0);
							Highter_Top();
							Put_Block();
							vTaskDelay(500);
							Open_Catcher();
							Circle_Saver(0, 0);
							Move_Right(1);
							return 1;
						}
				default:
					break;
			}
	return 0;
}


void Go_To_Rough_Area(void)
{
	Turning_Laps_T(80,1500,Turn_L);
	Direction_Initial(Turn_L);
	car.speed_T = 800;
	while(ADC_Value[3] < Anlog_Digtal)			// 原地旋转，将头的方向转向夹物台
			vTaskDelay(2);
	Stop();
	Direction_Initial(Left);
	TraceL_Turning_Laps(20,1000);
	TraceL_Turning_Laps(30,2000);
	Direction_Initial(Left);
	while(ADC_Value[1] < Anlog_Digtal)
		{
			TraceL_speed(800);
			vTaskDelay(2);
		}
	Stop();
	Turning_Laps(20,1500,Forward);
	TraceF_Turning_Laps(100,2000);
	Direction_Initial(Forward);
	while(ADC_Value[11] < Anlog_Digtal)
		{
			TraceF_speed(1500);
			vTaskDelay(2);
		}
	Stop();
}

void Move_Left_Rough(uint8_t position)
{
	switch(position)
		{
			case 0:
				Direction_Initial(Left);
				while(!DU1)
					{
						TraceL_speed(1200);
						vTaskDelay(2);
					}
//				Algment(3);
				Right_Aligment();
				Stop();
				break;
			case 1:
				TraceL_Turning_Laps(30, 1200);
				Direction_Initial(Left);
				while(ADC_Value[1] < Anlog_Digtal)
					{
						TraceL_speed(1200);
						vTaskDelay(2);
					}
				Stop();
				break;
			case 2:
				TraceL_Turning_Laps(80, 2000);
				Direction_Initial(Left);
				while(!DU1)
					{
						TraceL_speed(1200);
						vTaskDelay(2);
					}
				Stop();
				break;
			case 3:
				Direction_Initial(Left);
				while(!DU1)
					{
						TraceL_speed(1200);
						vTaskDelay(2);
					}
//				Algment(3);
				Left_Aligment();
				Stop();
			default:
				break;
		}
}

//void Trace_Left_Aligment(void)
//{
//	
//	
//}

//void Traace_Right_Aligment(void)
//{

//}

void Get_Block_Ground(uint8_t Block)
{
	switch(Block)
		{
			case 1:
				Turning_Laps(50,800,Back);
				Highter_Buttop();
				Highter_Buttonest();
				Turning_Laps(40,800,Forward);
				Close_Catcher();
				Highter_Top();
				Put_Block();
				Open_Catcher();
				Circle_Saver(1,1);
				Get_Block();
				Circle_Saver(2,0);
				break;
			case 2:
				Turning_Laps(50,800,Back);
				Highter_Buttop();
				Highter_Buttonest();
				Turning_Laps(40,800,Forward);
				Close_Catcher();
				Highter_Top();
				Put_Block();
				Open_Catcher();
				Circle_Saver(3,1);
				Get_Block();
				Circle_Saver(4,0);
				break;
			case 3:
				Turning_Laps(50,800,Back);
				Highter_Buttop();
				Highter_Buttonest();
				Turning_Laps(40,800,Forward);
				Close_Catcher();
				Highter_Top();
				Put_Block();
				Open_Catcher();
				Circle_Saver(0,0);
				break;
			default:
				break;
		}
}

void Put_Block_Ground(uint8_t Block)
{
	switch(Block)
		{
		case 1:
			Turning_Laps(30,800,Back);
			Close_Catcher();
			Get_Block();
			Circle_Saver(1,1);
			Highter_Buttop();
			Highter_Buttonest();
			vTaskDelay(800);
			Open_Catcher();
			vTaskDelay(500);
			Highter_Top();
			Put_Block();
			Circle_Saver(2,1);
			Turning_Laps(30,800,Forward);
			break;
		case 2:
			Turning_Laps(30,800,Back);
			Close_Catcher();
			Get_Block();
			Circle_Saver(3,1);
			Highter_Buttop();
			Highter_Buttonest();
			vTaskDelay(800);
			Open_Catcher();
			Highter_Top();
			Put_Block();
			Circle_Saver(4,1);
			Turning_Laps(30,800,Forward);
			break;
		case 3:
			Turning_Laps(30,800,Back);
			Close_Catcher();
			Get_Block();
			Circle_Saver(0,1);
			Highter_Buttop();
			Highter_Buttonest();
			vTaskDelay(800);
			Open_Catcher();
			Highter_Top();
			Turning_Laps(30,800,Forward);
			break;
		default:
			break;
		}
}

void Put_Last_Block_Ground(uint8_t Block)
{
	switch(Block)
		{
		case 1:
			Turning_Laps(70,800,Forward);
			Close_Catcher();
			Get_Block();
			Circle_Saver(1,1);
			Highter_Buttop();
			Highter_Buttonest();
			vTaskDelay(800);
			Open_Catcher();
			vTaskDelay(500);
			Highter_Top();
			Put_Block();
			Circle_Saver(2,1);
			Turning_Laps(70,800,Back);
			break;
		case 2:
			Turning_Laps(70,800,Forward);
			Close_Catcher();
			Get_Block();
			Circle_Saver(3,1);
			Highter_Buttop();
			Highter_Buttonest();
			vTaskDelay(800);
			Open_Catcher();
			Highter_Top();
			Put_Block();
			Circle_Saver(4,1);
			Turning_Laps(70,800,Back);
			break;
		case 3:
			Turning_Laps(70,800,Forward);
			Close_Catcher();
			Get_Block();
			Circle_Saver(0,1);
			Highter_Buttop();
			Highter_Buttonest();
			vTaskDelay(800);
			Open_Catcher();
			Highter_Top();
			Turning_Laps(70,800,Back);
			break;
		default:
			break;
		}
}

void Move_Right_Rough(uint8_t position)
{
	switch(position)
		{
			case 0:
				Direction_Initial(Right);
				while(ADC_Value[0] < Anlog_Digtal)
					{
						TraceR_speed(1200);
						vTaskDelay(2);
					}
				Stop();
				break;
			case 1:
				TraceR_Turning_Laps(50, 1200);
				Direction_Initial(Right);
				while(!DU2)
					{
						TraceR_speed(1200);
						vTaskDelay(2);
					}
				Stop();
				break;
			case 2:
//				TraceR_Turning_Laps(80, 1200);
				Direction_Initial(Right);
				while(!DU2)
					{
						TraceR_speed(1200);
						vTaskDelay(2);
					}
				Stop();
				break;
			default:
				break;
		}
}

void Put_Rough_First_Block(void)
{
	switch(UART2_RECV_BUF[4])
		{
			case '1':
				Move_Left_Rough(1);
				Stop();
//				Close_Catcher();
//				Open_Catcher();
				Put_Block_Ground(1);
				Stop();
				break;
			case '2':
				Move_Left_Rough(0);
				Algment(3);
				Left_Aligment();
				Put_Block_Ground(1);
				Stop();
//				Close_Catcher();
//				Open_Catcher();
				Stop();
				break;
			case '3':
				Move_Left_Rough(2);
				Algment(3);
				Left_Aligment();
				Put_Block_Ground(1);
//				Stop();
//				Close_Catcher();
//				Open_Catcher();
				Stop();
				break;
			default:
				break;
		}
}

void Put_Rough_Second_Block(void)
{
	switch(UART2_RECV_BUF[4])
		{
			case '1':
				switch(UART2_RECV_BUF[5])
					{
						case '2':
							Move_Right_Rough(2);		
							Algment(3);
							Left_Aligment();
							Stop();
							Put_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;
						case '3':
							Move_Left_Rough(3);		
							Algment(3);
							Left_Aligment();
							Stop();
							Put_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[5])
					{
						case '1':
							Move_Left_Rough(1);
							Put_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							Stop();
							break;
						case '3':
							Move_Left_Rough(2);
							Algment(3);
							Left_Aligment();
							Stop();
							Put_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							Stop();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[5])
					{
						case '1':
							Move_Right_Rough(0);
							Put_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							Stop();
							break;
						case '2':
							Move_Right_Rough(1);
							Algment(3);
							Left_Aligment();
							Stop();
							Put_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							Stop();
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Put_Rough_Third_Block(void)
{
	switch(UART2_RECV_BUF[5])
		{
			case '1':
				switch(UART2_RECV_BUF[6])
					{
						case '2':
							Move_Right_Rough(2);
							Algment(3);
							Left_Aligment();
							Stop();
							Put_Block_Ground(3);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							Stop();
							break;
						case '3':
							Move_Left_Rough(3);
							Algment(3);
							Left_Aligment();
							Stop();
							Put_Block_Ground(3);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[6])
					{
						case '1':
							Move_Left_Rough(1);
							Put_Block_Ground(3);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;
						case '3':
							Move_Left_Rough(2);
							Algment(3);
							Left_Aligment();
							Stop();
							Put_Block_Ground(3);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[6])
					{
						case '1':
							Move_Right_Rough(0);
							Put_Block_Ground(3);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;
						case '2':
							Move_Right_Rough(1);
							Algment(3);
							Left_Aligment();
							Stop();
							Put_Block_Ground(3);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Up_Put_Rough_First_Block(void)
{
	switch(UART2_RECV_BUF[0])
		{
			case '1':
				Move_Left_Rough(1);
				Stop();
//				Close_Catcher();
//				Open_Catcher();
				Put_Block_Ground(1);
				Stop();
				break;
			case '2':
				Move_Left_Rough(0);
				Algment(3);
				Left_Aligment();
				Put_Block_Ground(1);
//				Close_Catcher();
//				Open_Catcher();
				Stop();
				break;
			case '3':
				Move_Left_Rough(2);
				Algment(3);
				Left_Aligment();
				Put_Block_Ground(1);
//				Stop();
//				Close_Catcher();
//				Open_Catcher();
				Stop();
				break;
			default:
				break;
		}
}

void Up_Put_Rough_Second_Block(void)
{
	switch(UART2_RECV_BUF[0])
		{
			case '1':
				switch(UART2_RECV_BUF[1])
					{
						case '2':
							Move_Right_Rough(2);
							Algment(3);
							Left_Aligment();
							Put_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;
						case '3':
							Move_Left_Rough(3);
							Put_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[1])
					{
						case '1':
							Move_Left_Rough(1);
							Put_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							Stop();
							break;
						case '3':
							Move_Left_Rough(2);
							Algment(3);
							Left_Aligment();
							Put_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							Stop();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[1])
					{
						case '1':
							Move_Right_Rough(0);
							Put_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							Stop();
							break;
						case '2':
							Move_Right_Rough(1);
							Algment(3);
							Left_Aligment();
							Put_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							Stop();
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Up_Put_Rough_Third_Block(void)
{
	switch(UART2_RECV_BUF[1])
		{
			case '1':
				switch(UART2_RECV_BUF[2])
					{
						case '2':
							Move_Right_Rough(2);
							Algment(3);
							Left_Aligment();
							Put_Block_Ground(3);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							Stop();
							break;
						case '3':
							Move_Left_Rough(3);
							Algment(3);
							Left_Aligment();
							Put_Block_Ground(3);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[2])
					{
						case '1':
							Move_Left_Rough(1);
							Put_Block_Ground(3);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;
						case '3':
							Move_Left_Rough(2);
							Algment(3);
							Left_Aligment();
							Put_Block_Ground(3);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[2])
					{
						case '1':
							Move_Right_Rough(0);
							Put_Block_Ground(3);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;
						case '2':
							Move_Right_Rough(1);
							Algment(3);
							Left_Aligment();
							Put_Block_Ground(3);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Get_Rough_First_Block(void)
{
	switch(UART2_RECV_BUF[6])
		{
			case '1':
				switch(UART2_RECV_BUF[4])
					{
						case '2':
							Move_Right_Rough(2);
							Get_Block_Ground(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;
						case '3':
							Move_Left_Rough(3);
							Get_Block_Ground(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[4])
					{
						case '1':
							Move_Left_Rough(1);
							Get_Block_Ground(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						case '3':
							Move_Left_Rough(2);
							Get_Block_Ground(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[4])
					{
						case '1':
							Move_Right_Rough(0);
							Get_Block_Ground(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						case '2':
							Move_Right_Rough(1);
							Get_Block_Ground(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Get_Rough_Second_Block(void)
{
	switch(UART2_RECV_BUF[4])
		{
			case '1':
				switch(UART2_RECV_BUF[5])
					{
						case '2':
							Move_Right_Rough(2);
							Get_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						case '3':
							Move_Left_Rough(3);
							Get_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[5])
					{
						case '1':
							Move_Left_Rough(1);
							Get_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						case '3':
							Move_Left_Rough(2);
							Get_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[5])
					{
						case '1':
							Move_Right_Rough(0);
							Get_Block_Ground(2);
							Stop();
//							Open_Catcher();
//							Stop();
							break;
						case '2':
							Move_Right_Rough(1);
							Get_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Get_Rough_Third_Block(void)
{
	switch(UART2_RECV_BUF[5])
		{
			case '1':
				switch(UART2_RECV_BUF[6])
					{
						case '2':
							Move_Right_Rough(2);
							Get_Block_Ground(3);
							Move_Left(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						case '3':
							Move_Left_Rough(3);
							Get_Block_Ground(3);
							Move_Right(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[6])
					{
						case '1':
							Move_Left_Rough(1);
							Get_Block_Ground(3);
							Move_Right(0);
							Move_Left(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						case '3':
							Move_Left_Rough(2);
							Get_Block_Ground(3);
							Move_Right(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[6])
					{
						case '1':
							Move_Right_Rough(0);
							Get_Block_Ground(3);
							Move_Right(0);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						case '2':
							Move_Right_Rough(1);
							Get_Block_Ground(3);
							Move_Left(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Up_Get_Rough_First_Block(void)
{
	switch(UART2_RECV_BUF[2])
		{
			case '1':
				switch(UART2_RECV_BUF[0])
					{
						case '2':
							Move_Right_Rough(2);
							Get_Block_Ground(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
							break;
						case '3':
							Move_Left_Rough(3);
							Get_Block_Ground(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[0])
					{
						case '1':
							Move_Left_Rough(1);
							Get_Block_Ground(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						case '3':
							Move_Left_Rough(2);
							Get_Block_Ground(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[0])
					{
						case '1':
							Move_Right_Rough(0);
							Get_Block_Ground(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						case '2':
							Move_Right_Rough(1);
							Get_Block_Ground(1);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Up_Get_Rough_Second_Block(void)
{
	switch(UART2_RECV_BUF[0])
		{
			case '1':
				switch(UART2_RECV_BUF[1])
					{
						case '2':
							Move_Right_Rough(2);
							Get_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						case '3':
							Move_Left_Rough(3);
							Get_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[1])
					{
						case '1':
							Move_Left_Rough(1);
							Get_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						case '3':
							Move_Left_Rough(2);
							Get_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[1])
					{
						case '1':
							Move_Right_Rough(0);
							Get_Block_Ground(2);
							Stop();
//							Open_Catcher();
//							Stop();
							break;
						case '2':
							Move_Right_Rough(1);
							Get_Block_Ground(2);
							Stop();
//							Close_Catcher();
//							Open_Catcher();
//							Stop();
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Up_Get_Rough_Third_Block(void)
{
	switch(UART2_RECV_BUF[1])
		{
			case '1':
				switch(UART2_RECV_BUF[2])
					{
						case '2':
							Move_Right_Rough(2);
							Get_Block_Ground(3);
							Move_Left(1);
							Stop();
							break;
						case '3':
							Move_Left_Rough(3);
							Get_Block_Ground(3);
							Move_Right(1);
							Stop();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[2])
					{
						case '1':
							Move_Left_Rough(1);
							Get_Block_Ground(3);
							Move_Right(0);
							Move_Left(1);
							Stop();
							break;
						case '3':
							Move_Left_Rough(2);
							Get_Block_Ground(3);
							Move_Right(1);
							Stop();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[2])
					{
						case '1':
							Move_Right_Rough(0);
							Get_Block_Ground(3);
							Move_Right(0);
							Stop();
							break;
						case '2':
							Move_Right_Rough(1);
							Get_Block_Ground(3);
							Move_Left(1);
							Stop();
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Go_to_Last(void)
{
	TraceB_Turning_Laps(25,1000);
	TraceB_Turning_Laps(85,2500);
	TraceB_Turning_Laps(25,1500);
	Direction_Initial(Back);
	while(ADC_Value[3] < Anlog_Digtal)
		{
			TraceB_speed(1000);
			vTaskDelay(2);
		}
	Stop();
	Turning_Laps_T(80,1500,Turn_L);
	Direction_Initial(Turn_L);
	car.speed_T = 800;
	while(ADC_Value[1] < Anlog_Digtal)			// 原地旋转，将头的方向转向置物台
			vTaskDelay(2);
	Stop();
	TraceF_Turning_Laps(25,1000);
	TraceF_Turning_Laps(75,2500);
	TraceF_Turning_Laps(25,1500);
	Direction_Initial(Forward);
	while(ADC_Value[3] < Anlog_Digtal)
		{
			TraceF_speed(800);
			vTaskDelay(2);
		}
	Stop();
}

void Put_Last_First_Block(void)
{
	switch(UART2_RECV_BUF[4])
		{
			case '1':
				Move_Left_Rough(1);
				Stop();
				Put_Last_Block_Ground(1);
				Stop();
				break;
			case '2':
				Move_Left_Rough(0);
				Put_Last_Block_Ground(1);
				Stop();
				Stop();
				break;
			case '3':
				Move_Left_Rough(2);
				Put_Last_Block_Ground(1);
				Stop();
				break;
			default:
				break;
		}
}

void Put_Last_Second_Block(void)
{
	switch(UART2_RECV_BUF[4])
		{
			case '1':
				switch(UART2_RECV_BUF[5])
					{
						case '2':
							Move_Right_Rough(2);
							Put_Last_Block_Ground(2);
							Stop();
							break;
						case '3':
							Move_Left_Rough(3);
							Put_Last_Block_Ground(2);
							Stop();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[5])
					{
						case '1':
							Move_Left_Rough(1);
							Put_Last_Block_Ground(2);
							Stop();
							Stop();
							break;
						case '3':
							Move_Left_Rough(2);
							Put_Last_Block_Ground(2);
							Stop();
							Stop();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[5])
					{
						case '1':
							Move_Right_Rough(0);
							Put_Last_Block_Ground(2);
							Stop();
							Stop();
							break;
						case '2':
							Move_Right_Rough(1);
							Put_Last_Block_Ground(2);
							Stop();
							Stop();
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Put_Last_Third_Block(void)
{
	switch(UART2_RECV_BUF[5])
		{
			case '1':
				switch(UART2_RECV_BUF[6])
					{
						case '2':
							Move_Right_Rough(2);
							Put_Last_Block_Ground(3);
							Stop();
							Stop();
							break;
						case '3':
							Move_Left_Rough(3);
							Put_Last_Block_Ground(3);
							Stop();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[6])
					{
						case '1':
							Move_Left_Rough(1);
							Put_Last_Block_Ground(3);
							Stop();
							break;
						case '3':
							Move_Left_Rough(2);
							Put_Last_Block_Ground(3);
							Stop();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[6])
					{
						case '1':
							Move_Right_Rough(0);
							Put_Last_Block_Ground(3);
							Stop();
							break;
						case '2':
							Move_Right_Rough(1);
							Put_Last_Block_Ground(3);
							Stop();
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Down_Put_Last_Block_Ground(uint8_t Block)
{
	switch(Block)
		{
		case 1:
			Close_Catcher();
			Get_Block();
			Circle_Saver(1,1);
//			Highter_Buttop();
			Turning_Laps(60,800,Forward);
			Highter_Palletizing();
//			Highter_Buttonest();
			vTaskDelay(500);
			Open_Catcher();
			vTaskDelay(500);
			Turning_Laps(60,800,Back);
			Highter_Top();
			Put_Block();
			Circle_Saver(2,1);
			break;
		case 2:
			Close_Catcher();
			Get_Block();
			Circle_Saver(3,1);
//			Highter_Buttop();
			Turning_Laps(60,800,Forward);
			Highter_Palletizing();
//			Highter_Buttonest();
			vTaskDelay(500);
//			Turning_Laps(60,800,Forward);
			Open_Catcher();
			Turning_Laps(60,800,Back);
			Highter_Top();
			Put_Block();
			Circle_Saver(4,1);
			break;
		case 3:
			Close_Catcher();
			Get_Block();
			Circle_Saver(0,1);
//			Highter_Buttop();
			Turning_Laps(60,800,Forward);
			Highter_Palletizing();
//			Highter_Buttonest();
			vTaskDelay(500);
//			Turning_Laps(60,800,Forward);
			Open_Catcher();
			Turning_Laps(60,800,Back);
			Highter_Top();
			break;
		default:
			break;
		}
}

void Down_Put_Last_First_Block(void)
{
	switch(UART2_RECV_BUF[4])
		{
			case '1':
				Move_Left_Rough(1);
				Down_Put_Last_Block_Ground(1);
				Stop();
				break;
			case '2':
				Move_Left_Rough(0);
				Algment(3);
				Left_Aligment();
				Stop();
				Down_Put_Last_Block_Ground(1);
				Stop();
				break;
			case '3':
				Move_Left_Rough(2);
				Algment(3);
				Left_Aligment();
				Stop();
				Down_Put_Last_Block_Ground(1);
				Stop();
				break;
			default:
				break;
		}
}

void Down_Put_Last_Second_Block(void)
{
	switch(UART2_RECV_BUF[4])
		{
			case '1':
				switch(UART2_RECV_BUF[5])
					{
						case '2':
							Move_Right_Rough(2);
							Algment(3);
							Left_Aligment();
							Stop();
							Down_Put_Last_Block_Ground(2);
							Stop();
							break;
						case '3':
							Move_Left_Rough(3);
							Algment(3);
							Left_Aligment();
							Stop();
							Down_Put_Last_Block_Ground(2);
							Stop();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[5])
					{
						case '1':
							Move_Left_Rough(1);
							Down_Put_Last_Block_Ground(2);
							Stop();
							Stop();
							break;
						case '3':
							Move_Left_Rough(2);
							Algment(3);
							Left_Aligment();
							Stop();
							Down_Put_Last_Block_Ground(2);
							Stop();
							Stop();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[5])
					{
						case '1':
							Move_Right_Rough(0);
							Down_Put_Last_Block_Ground(2);
							Stop();
							Stop();
							break;
						case '2':
							Move_Right_Rough(1);
							Algment(3);
							Left_Aligment();
							Stop();
							Down_Put_Last_Block_Ground(2);
							Stop();
							Stop();
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Down_Put_Last_Third_Block(void)
{
	switch(UART2_RECV_BUF[5])
		{
			case '1':
				switch(UART2_RECV_BUF[6])
					{
						case '2':
							Move_Right_Rough(2);
							Algment(3);
							Left_Aligment();
							Stop();
							Down_Put_Last_Block_Ground(3);
							Stop();
							Stop();
							break;
						case '3':
							Move_Left_Rough(3);
							Algment(3);
							Left_Aligment();
							Stop();
							Down_Put_Last_Block_Ground(3);
							Stop();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[6])
					{
						case '1':
							Move_Left_Rough(1);
							Down_Put_Last_Block_Ground(3);
							Stop();
							break;
						case '3':
							Move_Left_Rough(2);
							Algment(3);
							Left_Aligment();
							Stop();
							Down_Put_Last_Block_Ground(3);
							Stop();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[6])
					{
						case '1':
							Move_Right_Rough(0);
							Down_Put_Last_Block_Ground(3);
							Stop();
							break;
						case '2':
							Move_Right_Rough(1);
							Algment(3);
							Left_Aligment();
							Stop();
							Down_Put_Last_Block_Ground(3);
							Stop();
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Up_Put_Last_First_Block(void)
{
	switch(UART2_RECV_BUF[0])
		{
			case '1':
				Move_Left_Rough(1);
				Stop();
				Put_Last_Block_Ground(1);
				Stop();
				break;
			case '2':
				Move_Left_Rough(0);
				Algment(3);
				Left_Aligment();
				Put_Last_Block_Ground(1);
				Stop();
				Stop();
				break;
			case '3':
				Move_Left_Rough(2);
				Algment(3);
				Left_Aligment();
				Put_Last_Block_Ground(1);
				Stop();
				break;
			default:
				break;
		}
}

void Up_Put_Last_Second_Block(void)
{
	switch(UART2_RECV_BUF[0])
		{
			case '1':
				switch(UART2_RECV_BUF[1])
					{
						case '2':
							Move_Right_Rough(2);
							Algment(3);
							Left_Aligment();
							Put_Last_Block_Ground(2);
							Stop();
							break;
						case '3':
							Move_Left_Rough(3);
							Algment(3);
							Left_Aligment();
							Put_Last_Block_Ground(2);
							Stop();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[1])
					{
						case '1':
							Move_Left_Rough(1);
							Put_Last_Block_Ground(2);
							Stop();
							Stop();
							break;
						case '3':
							Move_Left_Rough(2);
							Algment(3);
							Left_Aligment();
							Put_Last_Block_Ground(2);
							Stop();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[1])
					{
						case '1':
							Move_Right_Rough(0);
							Put_Last_Block_Ground(2);
							Stop();
							Stop();
							break;
						case '2':
							Move_Right_Rough(1);
							Algment(3);
							Left_Aligment();
							Put_Last_Block_Ground(2);
							Stop();
							Stop();
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Up_Put_Last_Third_Block(void)
{
	switch(UART2_RECV_BUF[1])
		{
			case '1':
				switch(UART2_RECV_BUF[2])
					{
						case '2':
							Move_Right_Rough(2);
							Algment(3);
							Left_Aligment();
							Put_Last_Block_Ground(3);
							Stop();
							Move_Right(1);
							break;
						case '3':
							Move_Left_Rough(3);
							Algment(3);
							Left_Aligment();
							Put_Last_Block_Ground(3);
							Move_Right(2);
							Move_Right(1);
							Stop();
							break;						
						default:
							break;
					}
				break;
			case '2':
				switch(UART2_RECV_BUF[2])
					{
						case '1':
							Move_Left_Rough(1);
							Put_Last_Block_Ground(3);
							Move_Right(1);
							Stop();
							break;
						case '3':
							Move_Left_Rough(2);
							Algment(3);
							Left_Aligment();
							Put_Last_Block_Ground(3);
							Move_Right(2);
							Move_Right(1);
							Stop();
							break;
						default:
							break;
					}
				break;
			case '3':
				switch(UART2_RECV_BUF[2])
					{
						case '1':
							Move_Right_Rough(0);
							Put_Last_Block_Ground(3);
							Move_Right(1);
							Stop();
							break;
						case '2':
							Move_Right_Rough(1);
							Algment(3);
							Left_Aligment();
							Put_Last_Block_Ground(3);
							Stop();
							Move_Right(1);
							break;
						default:
							break;
					}
				break;
			default:
				break;
		}
}

void Go_To_Saver(void)
{
	Turning_Laps_T(160,1500,Turn_L);
	Direction_Initial(Turn_L);
	car.speed_T = 800;
	while(ADC_Value[1] < Anlog_Digtal)			// 原地旋转，将头的方向转向置物台
			vTaskDelay(2);
	Stop();
	TraceF_Turning_Laps(25,1000);
	TraceF_Turning_Laps(190,2500);
	TraceF_Turning_Laps(25,1500);
	Direction_Initial(Forward);
	car.speed_L = 800;
	car.speed_R = 800;
	while(ADC_Value[11] < Anlog_Digtal)
		vTaskDelay(2);
	Stop();
}

void Begin(void)
{
//	car.drive=Forward;
//	car.speed_L=0;		
//	car.speed_R=0;
	Turning_Laps(50,3000,Forward);	// 向前走一段距离
	Direction_Initial(Forward);
	car.speed_L=2000;		
	car.speed_R=2000;
	while(ADC_Value[3] < Anlog_Digtal)			// 直到右边中间的灰度看到黑线，到达点（2，0）
			vTaskDelay(2);
	TraceR_Turning_Laps(150,2000);  // 向左巡线，巡到点（3，2）
	while(ADC_Value[9] < Anlog_Digtal)			
		{
			TraceR_speed(1500);
			vTaskDelay(2);
		}
	Turning_Laps_T(80,3000,Turn_R); // 原地一段距离旋转
	car.speed_T = 2000;
	while(ADC_Value[11] < Anlog_Digtal)			// 原地旋转，将头的方向转向夹物台
		vTaskDelay(2);
	TraceL_Turning_Laps(30,1000);	// 向左巡一段距离（低速）
	TraceL_Turning_Laps(80,2500);	// 向左巡一段距离（快速）
	while(ADC_Value[1] < Anlog_Digtal)			// 巡线，巡线到（5，2）
		{
			TraceL_speed(1500);
			vTaskDelay(2);
		}
	Stop();
}

void End(void)
{
	Turning_Laps(200,2000,Left);
	Turning_Laps(100,800,Left);
	Turning_Laps(150,2000,Forward);
}

