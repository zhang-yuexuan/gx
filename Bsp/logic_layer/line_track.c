#include "line_track.h"
#include "main.h"
#include "math.h"
#include "oled.h"
#include "stdio.h"
#include "stdlib.h"
//===================//=================================================================================================================================
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"
#include "bsp_can.h"
#include "maintask.h"
#include "maintask2.h"
//=================================================================================================================================
//GoL_Num
uint8_t place0L = 5;//0-1    3-1

uint8_t place1L_1 = 3;//1
uint8_t place1L_0 = 6;//1

uint8_t place2L_1 = 9;//2
uint8_t place2L_0 = 14;//2
//GoL_Num3
uint8_t place3L_1 = 7;//3
uint8_t place3L_0 = 12;//3
//GoL_Num2
uint8_t place1_2L = 13;//12
uint8_t place2_3L = 11;//23
uint8_t place1_2_3M= 9;///123zhong中线


//R
//GoR_Num
uint8_t place1R_1 = 5;//1
uint8_t place1R_0 = 12;//1

uint8_t place2R_1 = 11;//2
uint8_t place2R_0 = 10;//2
//GoR_Num3
uint8_t place3R_1 = 5;//3
uint8_t place3R_0 = 12;//3
//=================================================================================================================================



#define FB_P 0.3f  //FB 方向巡线P值
#define LR_P 0.35f  //LR 方向巡线P值

uint8_t r_plussM = 5;
uint8_t l_plussM = 5;//走到中线编码器夺多走一点点    1-2  2-3  1 2 3

uint8_t r_plussRL = 12;//走到边上编码器多走一点点  1 2 3 1-2  2-3
uint8_t l_plussRL = 8;

uint8_t Back_1 = 10;//二区域放第一个物块需要后退的编码器数

uint16_t High_speed =5000;  //最快速度
uint16_t High_speed1 = 2500;//

uint16_t High_speed2 = 3000;////2-3寻迹速度
uint16_t High_speed3 = 3000;////1-2寻迹速度

uint16_t up_speed = 2500;
uint16_t down_speed = 2500;

uint16_t bmq_speed = 3000;

uint16_t min_speed= 2000;  //满速卡最后一根线  
uint16_t alignment_speed= 1000;  //对齐线的速度  不寻迹的速度
uint16_t alignment_speed1= 1500;  //对齐线的速度 
uint16_t alittle_speed = 1000;
uint16_t LMR_SPEED = 1000;
uint16_t LMR_SPEED1 = 1500;

uint16_t CPQ_SPEED1 = 700;
uint16_t CPQ_SPEED2 = 700;






//===========================//=========================================================================================
uint8_t F_PLUSS1=5;
uint8_t F_PLUSS2=4;
uint8_t F_PLUSS3=3;
uint16_t F_PLUSS4=300;
uint16_t F_PLUSS5=400;
uint16_t F_PLUSS6=500;

uint16_t F_PLUSS7=600;
uint16_t F_PLUSS8=800;
uint16_t F_PLUSS9=1000;

uint16_t F_e7=300;
uint16_t F_e8=400;
uint16_t F_e9=500;



uint8_t B_PLUSS1=5;
uint8_t B_PLUSS2=4;
uint8_t B_PLUSS3=3;


uint8_t L_PLUSS1=5;
uint8_t L_PLUSS2=4;
uint8_t L_PLUSS5=3;

uint16_t L_PLUSS3=400;
uint16_t L_PLUSS4=500;

uint16_t L_PLUSS7=1000;
uint16_t L_PLUSS8=1200;
uint16_t L_PLUSS9=1400;

uint16_t L_e7=400;
uint16_t L_e8=600;
uint16_t L_e9=700;


uint8_t R_PLUSS1=6;
uint8_t R_PLUSS2=5;

uint8_t Huidu_F;
uint8_t Huidu_L;
uint8_t Huidu_R;
uint8_t Huidu_B;
uint8_t t=0;
uint8_t L_num=5;
uint8_t R_num=10;


extern uint8_t two_3_1;//标志位 //标记马上要捡东西了
extern uint16_t delay_tim ;//升降需要的时间
//===========================
// 车子状态
extern moto_info_t motor_info1[MOTOR_MAX_NUM];
extern All_car_Typedef car;
//static float track_error; //last_track_error;
extern int ch4[4];
/* ==============================================================
													定义小车方向
 * 往小车行驶方向看为正方向
	
								|=========F=====================											|
								|	 		  DU2		DU1 ↑	DUI9 		DUA4 |
								|	DUB0 		        ↑			     							DUA3     |		
								L-DUC2------      ↑-------  								DUA1						-R					
								|	DUB1 			      ↑												DUC1	    |
								|	DUC3			      ↑												DUA2	    |
								|				DUC0 DUC4	      ↑	DUC5	 DUA0        |
								|=========B=====================|
	
 ============================================================== */
 /* ------------------------------------------
	函数名： 			Turning_Laps_T()
	功能：编码器行走
	参数：fx：前进方向
	备注: 
------------------------------------------ */
 
 //void Turning_Laps_T(uint16_t num,float speed,car_Typedef fx)
// void Turning_Laps(uint16_t num,float speed,car_Typedef fx)//50/格
 
 /* ------------------------------------------
	函数名： 			Direction_Initial()
	功能：巡线初始化函数，初始化巡线参数，并初始化速度变量，设置前进方向
	参数： direction：前进方向
	备注: 黑色的值较小，白色的值较大
------------------------------------------ */

void Direction_Initial(car_Typedef direction)
{
	car.drive = direction;
	car.speed_L = 0;
	car.speed_R = 0;
}

void Stop(void)
{
	car.speed_L = 0;
	car.speed_R = 0;
	car.speed_T = 0;
	car.speed_LB = 0;
	car.speed_RB = 0;
	car.speed_RF = 0;
	car.speed_LF = 0;
}
/* ------------------------------------------
	函数名： 			Turning()
	功能：旋转(L,R)
	参数： speed：巡线速度
	备注: 黑色线返回值较小，白色返回值较大   黑色为1  白为0
------------------------------------------ */
void Turning(float speed,car_Typedef fx)//旋转
{
  car.drive = fx;
	car.speed_T = speed;
	

}
void Go_(float speed,car_Typedef fx)//不循序走
{
  car.drive = fx;
	car.speed_R = speed;
	car.speed_L = speed;

}
	
/* ------------------------------------------
	函数名： 			Tracex_sipeed()
	功能：条件巡线函数，x是巡线的方向(F,B,L,R)
	参数： speed：巡线速度
	备注: 黑色线返回值较小，白色返回值较大   黑色为1  白为0
------------------------------------------ */

void TraceF_speed1(uint16_t speed)//F寻迹
{
Huidu_F=(DU2<<3)|(DU1<<2)|(DUI9<<1)|DUA4; 
	car.drive=Forward1;

			switch(Huidu_F)
	{
		case 4:car.speed_LF = speed-F_e7;  car.speed_LB = speed;  car.speed_RB = speed;            
					 car.speed_RF = speed+F_PLUSS7;  ;break;       //0100
		case 2:car.speed_LF = speed+F_PLUSS7;  car.speed_LB = speed;  car.speed_RB = speed; 
					 car.speed_RF = speed-F_e7;  ;break;       //0010
		
		case 12:car.speed_LF =speed-F_e8; car.speed_LB = speed;  car.speed_RB = speed; 
					 car.speed_RF = speed+F_PLUSS8; break;        //1100
		case 3:car.speed_LF = speed+F_PLUSS8; car.speed_LB = speed;  car.speed_RB = speed; 
					 car.speed_RF = speed-F_e8; break;       //0011
		
		case 8:car.speed_LF = speed-F_e9; car.speed_LB = speed;  car.speed_RB = speed; 
					 car.speed_RF = speed+F_PLUSS9; break;        //1000
		case 1:car.speed_LF = speed+F_PLUSS9; car.speed_LB = speed;  car.speed_RB = speed; 
					 car.speed_RF = speed-F_e9; break;       //0001
		
		default:car.speed_LF = speed;car.speed_RF = speed;	 car.speed_LB = speed;  car.speed_RB = speed;      break;
			
		
//		case 4:car.speed_L = speed;            
//					 car.speed_R = speed+speed/F_PLUSS1;  ;break;       //0100
//		case 2:car.speed_L = speed+speed/F_PLUSS1; 
//					 car.speed_R = speed;  ;break;       //0010
//		case 12:car.speed_L =speed-100;
//					 car.speed_R = speed+speed/F_PLUSS2; break;        //1100
//		case 3:car.speed_L = speed+speed/F_PLUSS2;
//					 car.speed_R = speed-100; break;       //0011
//		case 8:car.speed_L =  speed-200;
//					 car.speed_R = speed+speed/F_PLUSS3; break;        //1000
//		case 1:car.speed_L = speed+speed/F_PLUSS3;
//					 car.speed_R = speed-200; break;       //0001
//		
//		default:car.speed_L = speed;car.speed_R = speed; break;
//	
	

	
	}
	
	
	
	
}
void TraceF_speed(uint16_t speed)
{

	//DUA4   DUI9  DU1  DU2
	//DU2  DU1  DUI9 DUA4
		Huidu_F=(DU2<<3)|(DU1<<2)|(DUI9<<1)|DUA4; 
	  car.drive=Forward;
		switch(Huidu_F)
	{
		case 4:car.speed_L = speed;            
					 car.speed_R = speed+F_PLUSS4;  ;break;       //0100
		case 2:car.speed_L = speed+F_PLUSS4; 
					 car.speed_R = speed;  ;break;       //0010
		
		case 12:car.speed_L =speed;
					 car.speed_R = speed+F_PLUSS5; break;        //1100
		case 3:car.speed_L = speed+F_PLUSS5;
					 car.speed_R = speed; break;       //0011
		
		case 8:car.speed_L = speed-100;
					 car.speed_R = speed+F_PLUSS6; break;        //1000
		case 1:car.speed_L = speed+F_PLUSS6;
					 car.speed_R = speed-100; break;       //0001
		
		default:car.speed_L = speed;car.speed_R = speed; break;
		
	
		
//		case 4:car.speed_L = speed;            
//					 car.speed_R = speed+speed/F_PLUSS1;  ;break;       //0100
//		case 2:car.speed_L = speed+speed/F_PLUSS1; 
//					 car.speed_R = speed;  ;break;       //0010
//		case 12:car.speed_L =speed;
//					 car.speed_R = speed+speed/F_PLUSS2; break;        //1100
//		case 3:car.speed_L = speed+speed/F_PLUSS2;
//					 car.speed_R = speed; break;       //0011
//		case 8:car.speed_L =  speed;
//					 car.speed_R = speed+speed/F_PLUSS3; break;        //1000
//		case 1:car.speed_L = speed+speed/F_PLUSS3;
//					 car.speed_R = speed; break;       //0001
//		
//		default:car.speed_L = speed;car.speed_R = speed; break;
//	
//	
	
	}
	
	
	
	
	
}
void TraceL_speed(uint16_t speed)
{/*
		DUB0
	
  	DUC2
	  DUB1
		DUC3	  	
	
	*/

			Huidu_L=(DUC3<<3)|(DUB1<<2)|(DUC2<<1)|DUB0; 
	
/*	car.drive=Left;
					switch(Huidu_L)
	{
		case 4:car.speed_L = speed;            
					 car.speed_R = speed+speed/L_PLUSS1;  ;break;       //0100
		case 2:car.speed_L = speed+speed/L_PLUSS1; 
					 car.speed_R = speed;  ;break;       //0010
		case 12:car.speed_L =speed;
					 car.speed_R = speed+speed/L_PLUSS2; break;        //1100
		case 3:car.speed_L = speed+speed/L_PLUSS2;
					 car.speed_R = speed; break;       //0011
		case 8:car.speed_L =  speed;
					 car.speed_R = speed+speed/L_PLUSS5; break;        //1000
		case 1:car.speed_L = speed+speed/L_PLUSS5;
					 car.speed_R = speed; break;       //0001
		
		default:car.speed_L = speed;car.speed_R = speed; break;
		

	}
	*/
	  car.drive=Left1;

				switch(Huidu_L)
	{
		case 4:car.speed_LF = speed-L_e7;       car.speed_LB = speed;  car.speed_RB = speed;        
					 car.speed_RF = speed+L_PLUSS7;  ;break;       //0100
		case 2:car.speed_LF = speed+L_PLUSS7;  car.speed_LB = speed;  car.speed_RB = speed;  
					 car.speed_RF = speed-L_e7;  ;break;       //0010
		
		
		
		
		case 12:car.speed_LF =speed-L_e8; car.speed_LB = speed;  car.speed_RB = speed;  
					 car.speed_RF = speed+L_PLUSS8; break;        //1100
		
		case 3:car.speed_LF = speed+L_PLUSS8; car.speed_LB = speed;  car.speed_RB = speed;  
					 car.speed_RF = speed-L_e8; break;       //0011
		
		
		
		case 8:car.speed_LF = speed-L_e9; car.speed_LB = speed;  car.speed_RB = speed;  
					 car.speed_RF = speed+L_PLUSS9; break;        //1000
		case 1:car.speed_LF = speed+L_PLUSS9; car.speed_LB = speed;  car.speed_RB = speed;  
					 car.speed_RF = speed-L_e9; break;       //0001
		
		default:car.speed_LF = speed;car.speed_RF = speed;	 car.speed_LB = speed;  car.speed_RB = speed;      break;

	}
	

}
void TraceB_speed(uint16_t speed)
{
		Huidu_B=(DUA0<<3)|(DUC5<<2)|(DUC4<<1)|DUC0; 
	  car.drive=Back;
		switch(Huidu_B)
	{
		case 4:car.speed_L = speed;            
					 car.speed_R = speed+speed/B_PLUSS1;  ;break;       //0100
		case 2:car.speed_L = speed+speed/B_PLUSS1; 
					 car.speed_R = speed;  ;break;       //0010
		case 12:car.speed_L =speed;
					 car.speed_R = speed+speed/B_PLUSS2; break;        //1100
		case 3:car.speed_L = speed+speed/B_PLUSS2;
					 car.speed_R = speed; break;       //0011
		case 8:car.speed_L =  speed;
					 car.speed_R = speed+speed/B_PLUSS3; break;        //1000
		case 1:car.speed_L = speed+speed/B_PLUSS3;
					 car.speed_R = speed; break;       //0001
		
		default:car.speed_L = speed;car.speed_R = speed; break;
	}
	
	
	
}



void TraceR_speed(uint16_t speed)
{
/*
  DUA3
	
	DUA1
	DUC1
	DUA2 	
	*/
				Huidu_L=(DUA1<<2)|(DUC1<<1)|DUA2; 
	  car.drive=Right;
		switch(Huidu_L)
	{
		case 6:car.speed_L = speed;            
					 car.speed_R = speed+speed/R_PLUSS1;  ;break;       //110
		case 3:car.speed_L = speed+speed/R_PLUSS1; 
					 car.speed_R = speed;  ;break;       //011
		case 4:car.speed_L = speed;            
					 car.speed_R = speed+speed/R_PLUSS2;  ;break;       //100
		case 1:car.speed_L = speed+speed/R_PLUSS2; 
					 car.speed_R = speed;  ;break;       //001
		
		default:car.speed_L = speed;car.speed_R = speed; break;
	}
	
	
	
}

/* ------------------------------------------
	函数名： 			Go_F_Turning_Laps()
	功能：编码器向前数线
	参数： speed：速度
	备注: 可以不需要再线上
------------------------------------------ */
//void Go_F_Turning_Laps(uint16_t num,float speed)//编码器数线
//{     
//	int32_t s = 0;
//	int32_t stang = 0;
//	car.speed_L=speed;
//	car.speed_R=speed;
//	car.drive=Forward;
//	while(1){
//		err=abs(motor_info1[0].rotor_angle-stang);
//		if(err>7500){
//				s=s+1;
//				TraceF_speed(speed);
//		}
//		if(s>=num){
//				car.speed_L=0;		
//				car.speed_R=0;
//				break;
//		}
//		vTaskDelay(2);
//	}
//}



void TraceF_Turning_Laps(uint16_t num,float speed)//编码器巡线
{     
	int32_t s = 0;
	int32_t stang = 0;
	car.speed_L=speed;
	car.speed_R=speed;
	car.drive=Forward;
	while(1){
		err=abs(motor_info1[0].rotor_angle-stang);
		if(err>7500){
				s=s+1;
				TraceF_speed(speed);
		}
		if(s>=num){
				car.speed_L=0;		
				car.speed_R=0;
				break;
		}
		vTaskDelay(2);
	}
}

void TraceB_Turning_Laps(uint16_t num,float speed)
{     
//	int32_t s = 0;
//	int32_t stang = 0;
//	car.speed_L=speed;
//	car.speed_R=speed;
//	car.drive=Back;
//	while(1){
//		err=abs(motor_info1[0].rotor_angle-stang);
//		if(err>7500){
//				s=s+1;
//				TraceB_speed(speed);
//		}
//		if(s>=num){
//				car.speed_L=0;		
//				car.speed_R=0;
//				break;
//		}
//		vTaskDelay(2);
//	}
}

void TraceL_Turning_Laps(uint16_t num,float speed)
{     
//	int32_t s = 0;
//	int32_t stang = 0;
//	car.speed_L=speed;
//	car.speed_R=speed;
//	car.drive=Left;
//	while(1){
//		err=abs(motor_info1[0].rotor_angle-stang);
//		if(err>7500){
//				s=s+1;
//				TraceL_speed(speed);
//		}
//		if(s>=num){
//				car.speed_L=0;		
//				car.speed_R=0;
//				break;
//		}
//		vTaskDelay(2);
//	}
}

void TraceR_Turning_Laps(uint16_t num,float speed)
{     
//	int32_t s = 0;
//	int32_t stang = 0;
//	car.speed_L=speed;
//	car.speed_R=speed;
//	car.drive=Right;
//	while(1){
//		err=motor_info1[0].rotor_angle-stang;
//		if(err>7500){
//				s=s+1;
//				TraceR_speed(speed);
//		}
//		if(s>=num){
//				car.speed_L=0;		
//				car.speed_R=0;
//				break;
//		}
//		vTaskDelay(2);
//	}
}
 /*============================================================== */
/* ------------------------------------------
	函数名： 			Tracex_sipeed()
	功能：数线
	参数： num  线的数量
	备注: 黑色线返回值较小，白色返回值较大   黑色为1  白为0
------------------------------------------ */
void TraceR_Num(uint16_t num,float speed,uint8_t mode)//寻迹数线
{
	int8_t  u=0;
	Turning_Laps(10,min_speed,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
	//TraceF_Turning_Laps(15,3000);
	if(mode == 0)
	while(num)
		{
				if(num==1)TraceR_speed(min_speed);
				else TraceR_speed(speed);			
		if(DUA3 ==1)//LEFT3
		{
			HAL_Delay(5);
			if(DUA3 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	else if(mode==1)
	{
		while(num)
		{
				if(num==1)TraceR_speed(min_speed);
				else TraceR_speed(speed);			
		if(DUI9 ==1)//LEFT3
		{
			HAL_Delay(5);
			if(DUI9 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	
	}
		
		
	Stop();


}
void TraceF_Num(uint16_t num,float speed)
{
	if(car_palce==30)
	{
		int8_t  u=0;
	Turning_Laps(15,2000,Forward);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
	
	//TraceF_Turning_Laps(15,3000);
	while(num)
		{
				if(num==1)TraceF_speed(speed);
				else TraceF_speed(speed);
				
		if(DUB1==1)//LEFT3
		{
			HAL_Delay(5);
			if(DUB1==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;


	}
	
	}
	
	else
	{
	
	int8_t  u=0;
	Turning_Laps(15,2000,Forward);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
	
	//TraceF_Turning_Laps(15,3000);
	while(num)
		{
				if(num==1)TraceF_speed(min_speed);
				else TraceF_speed(speed);
				
		if(DUB1==1)//LEFT3
		{
			HAL_Delay(5);
			if(DUB1==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;


	}
		}
	Stop();

}
void TraceB_Num(uint16_t num,float speed)
{
	int8_t  u=0;
	Turning_Laps(15,2000,Back);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
	//TraceF_Turning_Laps(15,3000);
	while(num)
		{
				if(num==1)TraceB_speed(min_speed);
				else TraceB_speed(speed);
		if(DUB1==1)//LEFT3
		{
			HAL_Delay(5);
			if(DUB1==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	Stop();

}
void TraceF_FAST_Num(uint16_t num,float speed)//寻迹数线
{
	int8_t  u=0;
		while(num)
		{
   TraceF_speed1(speed);			
		if(DUI0 ==1)//LEFT3
		{
			HAL_Delay(5);
			if(DUI0 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		
		}
		
	
	Stop();

}
void TraceL_FAST_Num(uint16_t num,float speed)//寻迹数线
{	
	int8_t  u=0;
		while(num)
		{
   TraceL_speed(speed);			
		if(DUI0 ==1)//LEFT3
		{
		HAL_Delay(10);
			if(DUI0 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		
		}

	
	Stop();
}
void TraceL_Num_Fjg(uint16_t num,float speed,uint8_t mode)//前激光数线
{
int8_t  u=0;
		if(mode == 0)
	while(num)
		{
      TraceL_speed(speed);			
		if(DUI0 ==1)//LEFT3
		{
		HAL_Delay(2);
			if(DUI0 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;
		


		}
			Stop();

}
void TraceL_Num_Ljg(uint16_t num,float speed,uint8_t mode)//左数线 mode：0 停在空格上  1 线上
{
int8_t  u=0;
		if(mode == 0)
	while(num)
		{
      TraceL_speed(speed);			
		if(DUI7 ==1)//LEFT3
		{
		HAL_Delay(3);
			if(DUI7 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;
		


		}
		else	if(mode == 1)	
		{
			while(num)
		{
      TraceL_speed(speed);			
		if(DUI2 ==1)//LEFT3
		{
		HAL_Delay(2);
			if(DUI2 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;
		


		}
		
		
		}
		
		
			Stop();

}
void TraceL_Num(uint16_t num,float speed,uint8_t mode)
{
	int8_t  u=0;
	Turning_Laps(15,2000,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
	//TraceF_Turning_Laps(15,3000);
	if(mode == 0)
	while(num)
		{
				if(num==1)TraceL_speed(2000);
				else TraceL_speed(speed);			
		if(DUB0 ==1)//LEFT3
		{
			HAL_Delay(3);
			if(DUB0 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;
		Turning_Laps(3,2000,Right);//编码器行走前后左右   编码器旋转   或  斜走


		}
	else if(mode==1)
	{
		while(num)
		{
			TraceL_speed(speed);			
		if(DU1 ==1)//LEFT3
		{
			HAL_Delay(5);
			if(DU1 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		
		}
		Turning_Laps(3,2000,Left);//编码器行走前后左右   编码器旋转   或  斜走
	
	}
		
		
	Stop();

}
void GoL_Num1(uint16_t num,float speed,uint8_t mode)//不寻迹数线  速度要慢
{


	int8_t  u=0;
//	 
	//TraceF_Turning_Laps(15,3000);
	if(mode == 0)
	{	
		if(num>1)
		{
	Turning_Laps(30,2000,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
			num--;
		}
		else if(num==1){
			Turning_Laps(15,2000,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		}
			
		while(num)
		{
	
					Go_(speed,Left);			
		if(DUB1 ==1)//LEFT3
		{
			HAL_Delay(5);
			if(DUB1 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		Turning_Laps(5,2000,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
	}
	else if(mode==1)
	{
		Turning_Laps(15,2000,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		while(num)
		{

					Go_(speed,Left);				
		if(DU1 ==1)//LEFT3
		{
			HAL_Delay(3);
			if(DU1 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		
	Turning_Laps( 10,1000,Left);
	}
		
		
	Stop();

}
void GoB_Num_Bjg(uint16_t num,float speed )//HOU激光数线
{

	int8_t  u=0;
		while(num)
		{
	
					Go_(speed,Back);			
		if(DUI7 ==1)//LEFT3
		{

HAL_Delay(3);
			
			if(DUI7 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		
		
		num=1;u=0;
		while(num)
		{

		Go_(1000,Back);				
		if(DUB1 ==1)//LEFT3
		{
			HAL_Delay(3);
			if(DUB1 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		

			Stop();
}
void Trace_L_Num_30(uint16_t num,float speed )//后激光数线  左走
{
	int8_t  u=0;
		while(num)
		{
	
					TraceL_speed(speed);			
		if(DUI0 ==1)//LEFT3
		{
			HAL_Delay(3);
			if(DUI0 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		
		
		num=1;u=0;
		
		
		while(num)
		{

					Go_(4000,Left);				
		if(DU1 ==1||DUI9==1)//LEFT3
		{
			HAL_Delay(3);
			if(DU1 ==1||DUI9==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		
	Turning_Laps( 10,1000,Left);
			Stop();
}
void Trace_L_Num_Bjg(uint16_t num,float speed )//前激光数线
{
	int8_t  u=0;
		while(num)
		{
	
					TraceL_speed(speed);			
		if(DUI0 ==1)//LEFT3
		{
			HAL_Delay(3);
			if(DUI0 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		
		
		num=1;u=0;
		
		
		while(num)
		{

					Go_(2000,Left);				
		if(DU1 ==1)//LEFT3
		{
			HAL_Delay(3);
			if(DU1 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		
	Turning_Laps( 10,1000,Left);
			Stop();

}



void GoL_Num2(uint16_t num,float speed,uint8_t mode)//1-2
{
	int8_t  u=0;
		if(mode == 0)
	{
		
			
		while(num)
		{
	
					Go_(speed,Left);			
		if(DUH11 ==1)//LEFT3
		{
			HAL_Delay(3);
			if(DUH11 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		
		if(car_palce==12)
	Turning_Laps( place1_2L,1000,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		else if(car_palce==23)
		Turning_Laps(place2_3L,1000,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		
	}
		else if(mode==1)
	{
		while(num)
		{

					Go_(speed,Left);				
		if(DU1 ==1)//LEFT3
		{
			HAL_Delay(3);
			if(DU1 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		
	Turning_Laps( place1_2_3M,1000,Left);
	}
		
		
	Stop();
	
}
void GoL_Num3(uint16_t num,float speed,uint8_t mode)//不寻迹数线  速度要慢
{
	int8_t  u=0;

	
	if(mode == 0)
	{
		if(num>1)
		{
		Turning_Laps(70,1500,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
			num--;
		}
		else if(num==1){
				Turning_Laps(30,1500,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		}
			
		while(num)
		{
	
					Go_(speed,Left);			
		if(DUC1 ==1)//LEFT3
		{
			HAL_Delay(5);
			if(DUC1 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
			Turning_Laps( place3L_0,1000,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
//	Turning_Laps(5,2000,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
	}
	else if(mode==1)
	{
				if(DU2==1||		DU1==1|| 	DUI9==1|| DUA4==1)
		Turning_Laps(30,1500,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
	
		
		while(num)
		{

					Go_(speed,Left);				
		if(DU1 ==1)//LEFT3
		{
			HAL_Delay(3);
			if(DU1 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		
	Turning_Laps( place3L_1,1000,Left);
	}
		
		
	Stop();
	

}	

void GoB_Num(uint16_t num,float speed )//不寻迹数线  速度要慢
{
	int8_t  u=0;
		while(num)
		{
	
					Go_(speed,Back);			
		if(DUB1==1)//LEFT3
		{
			HAL_Delay(5);
			if(DUB1==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
				
	Stop();
}

/////////////////////////下L///////////////////////////////////////////////////////////////////////////////////////////
void GoR_Num(uint16_t num,float speed,uint8_t mode)//
{
	int8_t  u=0;
	//	 

	//TraceF_Turning_Laps(15,3000);
	if(mode == 0)
	{
		if(num>1)
		{		
			if(car_palce==1&&updowm==1)Turning_Laps(40,2000,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
			else if(car_palce==1&&updowm==0)Turning_Laps(40,2000,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
			else if(car_palce==2&&two_3_1==0)Turning_Laps(40,3000,Right);
			else Turning_Laps(40,1500,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;

			num--;
		}
				else if(num==1){
					if(car_palce==1&&updowm==1)	Turning_Laps(20,2000,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
					else if(car_palce==1&&updowm==0)Turning_Laps(20,2000,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		      else	Turning_Laps(20,1500,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		}
				
		
		
		
		while(num)
		{
//				if(num==1)Go_(alignment_speed,Right);//不寻迹走
//				else 
					Go_(speed,Right);			
		if(DUB1 ==1)//LEFT3
		{
			HAL_Delay(10);
			if(DUB1 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}

		if(car_palce==2) Turning_Laps(place2R_0,1000,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		else Turning_Laps(place1R_0,1000,Right);	
		
	}
	else if(mode==1)
	{	
//		if(DU2==1||		DU1==1|| 	DUI9==1|| DUA4==1)
//		Turning_Laps(20,1500,Right);	// 
		
				if(	car_palce==1||car_palce==3||car_palce==12)
		if(DU2==1||		DU1==1|| 	DUI9==1|| DUA4==1)
		Turning_Laps(20,1500,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		
		if(	car_palce==2)
		if(DUC0==1||		DUC4==1|| 	DUC5==1|| DUA0==1)//DUC0 DUC4	      ↑	DUC5	 DUA0 
		Turning_Laps(20,1500,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		
		
		
			if(	car_palce==2)
	{
			while(num)
		{
//				if(num==1)Go_(alignment_speed,Right);//不寻迹走
//				else
					Go_(speed,Right);				
		if(DUC5 ==1)//LEFT3
		{
			HAL_Delay(10);
			if(DUC5 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	
	}
	else{
		while(num)
		{
//				if(num==1)Go_(alignment_speed,Right);//不寻迹走
//				else
					Go_(speed,Right);				
		if(DUI9 ==1)//LEFT3
		{
			HAL_Delay(10);
			if(DUI9 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	}
	if(car_palce==1)Turning_Laps( place1R_1,1000,Right);
	else Turning_Laps( place2R_1,1000,Right);
	}
		
		
	Stop();
}
/////////////////////////上R///////////////////////////////////////////////////////////////////////////////////////////
void GoL_Num(uint16_t num,float speed,uint8_t mode)//不寻迹数线   mode=0时num不能大于2
{
	int8_t  u=0;

	//TraceF_Turning_Laps(15,3000);
	if(mode == 0)
	{
		if(num>1)
		{
		if(car_palce==1&&updowm==1)Turning_Laps(30,2000,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		else if(car_palce==1&&updowm==0)Turning_Laps(30,2000,Left);
			else if(car_palce==2&&two_3_1==0)Turning_Laps(40,3000,Left);
		else Turning_Laps(40,1500,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
			num--;
		}
		else if(num==1){
		if(car_palce==1&&updowm==1)	Turning_Laps(20,2000,Left);
		else if(car_palce==1&&updowm==0)Turning_Laps(20,2000,Left);
		else if(car_palce==0);
		
		else	Turning_Laps(20,1500,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		}
			
		while(num)
		{
	
		Go_(speed,Left);			
		if(DUC1 ==1)//LEFT3
		{
			HAL_Delay(10);
			if(DUC1 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		

		if(car_palce==1&&updowm==1)Turning_Laps(place1L_0,1000,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		else if(car_palce==1&&updowm==0)Turning_Laps(place1L_0,1000,Left);
		
		else if(car_palce==31||car_palce==0)Turning_Laps(place0L,1000,Left);

		else if(car_palce==2)Turning_Laps(place2L_0,1000,Left);	
		else Turning_Laps(10,1000,Left);	
		
	}
	else if(mode==1)
	{
		if(	car_palce==1||car_palce==3||car_palce==12)
		if(DU2==1||		DU1==1|| 	DUI9==1|| DUA4==1)
		Turning_Laps(20,1500,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		
		if(	car_palce==2)
		if(DUC0==1||		DUC4==1|| 	DUC5==1|| DUA0==1)//DUC0 DUC4	      ↑	DUC5	 DUA0 
		Turning_Laps(20,1500,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		
		
		
	if(	car_palce==2||car_palce==23||car_palce==30)
	{
			while(num)
		{

					Go_(speed,Left);				
		if(DUC4 ==1)//LEFT3  yong用后面的灰度
		{
			HAL_Delay(10);
			if(DUC4 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	
	}
	else if(car_palce==30)
	{
				while(num)
		{

					Go_(speed,Left);				
		if(DU1 ==1||DUI9==1||DU2==1||DUA4==1)//LEFT3
		{
			HAL_Delay(5);
		if(DU1 ==1||DUI9==1||DU2==1||DUA4==1)//LEFT3
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	
	
	}
		else
		{
		while(num)
		{

					Go_(speed,Left);				
		if(DU1 ==1||DUI9==1)//LEFT3
		{
			HAL_Delay(10);
			if(DU1 ==1||DUI9==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	}
		
		
		
//	if(car_palce==1||car_palce==0||car_palce==31)Turning_Laps( 3,1000,Left);
	if(car_palce==1)Turning_Laps( place1L_1,1000,Left);
	else if(car_palce==2)Turning_Laps(  place2L_1,1000,Left);
	else if(car_palce==30);
	else Turning_Laps( 9,1000,Left);
	}
		
		
	Stop();

}

void GoR_Num3(uint16_t num,float speed,uint8_t mode)//
{

	int8_t  u=0;
	
	//TraceF_Turning_Laps(15,3000);
	if(mode == 0)
	{
		if(num>1)
		{
				Turning_Laps(70,1500,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;

			num--;
		}
				else if(num==1){
			Turning_Laps(30,1500,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		}
		while(num)
		{
//				if(num==1)Go_(alignment_speed,Right);//不寻迹走
//				else 
					Go_(speed,Right);			
		if(DUB1 ==1)//LEFT3
		{
			HAL_Delay(5);
			if(DUB1 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		Turning_Laps( place3R_0,1000,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
	}
	else if(mode==1)
	{	
						if(DU2==1||		DU1==1|| 	DUI9==1|| DUA4==1)
		Turning_Laps(30,1500,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;

		while(num)
		{
//				if(num==1)Go_(alignment_speed,Right);//不寻迹走
//				else
					Go_(speed,Right);				
		if(DU1 ==1)//LEFT3
		{
			HAL_Delay(5);
			if(DU1 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		
	Turning_Laps(  place3R_1,1000,Left);
	}
		
		
	Stop();

}
void GoR_Num1(uint16_t num,float speed,uint8_t mode)//
{	int8_t  u=0;
	
	//TraceF_Turning_Laps(15,3000);
	if(mode == 0)
	{
		if(num>1)
		{
		Turning_Laps(30,speed,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
			num--;
		}
				else if(num==1){
			Turning_Laps(15,speed,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		}
		while(num)
		{
//				if(num==1)Go_(alignment_speed,Right);//不寻迹走
//				else 
					Go_(speed,Right);			
		if(DUA1 ==1)//LEFT3
		{
			HAL_Delay(5);
			if(DUA1 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		Turning_Laps(7,1000,Left);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
	}
	else if(mode==1)
	{
		Turning_Laps(15,1000,Right);	// 向前zou走一点，走出黑线避免多数一根 L1:DUB0;
		while(num)
		{
//				if(num==1)Go_(alignment_speed,Right);//不寻迹走
//				else
					Go_(speed,Right);				
		if(DU1 ==1)//LEFT3
		{
			HAL_Delay(5);
			if(DU1 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		
	Turning_Laps( 5,1000,Left);
	}
		
		
	Stop();
}

/* ------------------------------------------
	函数名： 			Turn_Num()
	功能：旋转
	参数： num  线的数量  speed 速度    fx 旋转方向   fx1 旋转后哪个方向卡住线
	备注: 黑色线返回值较小，白色返回值较大   黑色为1  白为0
------------------------------------------ */
void Turn_Num(uint16_t num,float speed,car_Typedef fx,car_Typedef fx1)
{
	int8_t  u=0;
//=====================================头朝前	=============================
	if(fx==Turn_L&&fx1==Forward)
	{
		num=2;
//		Turning_Laps_T(70,bmq_speed,Turn_L);	// 
	
				while(num)
		{
				Turning(speed,Turn_L);//旋转
		if(DUI0==1)//F2
		{
//			HAL_Delay(2);
			if(DUI0==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		
		num=1;u=0;
		while(num)
		{
			
				Turning(2000,Turn_L);//旋转
		if(DU1==1)//F2
		{
			HAL_Delay(3);
			if(DU1==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}	
		

//					int8_t stateL=0,stateR=0;
//	car.drive = Turn_L1;

//		
//		while(1)
//	{
//	if(stateR==0)
//	{
//	if(DUC1==1)
//	{
////		HAL_Delay(2);
//		if(DUC1==1)
//	{
//	car.speed_R = 0;
//	stateR=1;}
//	}
//	else car.speed_R = speed;
// }else car.speed_R = 0;
////==========================	
//	if(stateL==0)
//	{
//		if(DUC2==1)
//	{
////		HAL_Delay(2);
//		if(DUC2==1)
//	{	
//	car.speed_L = 0;
//	stateL=1;}
//	}
//	else car.speed_L = speed;
//}else car.speed_L = 0;
//	if (stateL!=0&&stateR!=0)break;
//	
// }
	
		
//	Turning_Laps_T(10,1000,Turn_L);	// 旋转一点
		}
else	if(fx==Turn_L&&fx1==Left)
	{
//	car.drive = Forward;
//	car.speed_R = speed;
//	car.speed_L = -speed;
//		 Turning_Laps(25,bmq_speed,Turn_L);//68/90度
	car.drive = Turn_L;		
			int8_t  u=0;
		num=1;
				while(num)
		{
				Turning(5000,Turn_L);//旋转
		if(DUI0==1)//F2
		{
//			HAL_Delay(2);
			if(DUI0==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}	
		
		
		
		u=0;num=1;
		
		
			int8_t stateL=0,stateR=0;
	car.drive = Turn_L1;

		
		while(1)
	{
	if(stateR==0)
	{
	if(DUC1==1)
	{
//		HAL_Delay(2);
		if(DUC1==1)
	{
	car.speed_R = 0;
	stateR=1;}
	}
	else car.speed_R = speed;
 }else car.speed_R = 0;
//==========================	
	if(stateL==0)
	{
		if(DUC2==1)
	{
//		HAL_Delay(2);
		if(DUC2==1)
	{	
	car.speed_L = 0;
	stateL=1;}
	}
	else car.speed_L = speed;
}else car.speed_L = 0;
	if (stateL!=0&&stateR!=0)break;
	
 }
	
		
		

		}	
		
		
else	if(fx==Turn_R&&fx1==Forward)
	{
	Turning_Laps_T(10,1000,Turn_R);	// 旋转一点，走出黑线避免多数一根 L1:DUB0;
	while(num)
		{

				Turning(speed,Turn_R);//旋转
		if(DUI9==1)//F3
		{
			HAL_Delay(5);
			if(DUI9==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
		Turning_Laps_T(10,1000,Turn_R);	// 旋转一点
		}	
		
		
		

	Stop();

}




void alignmentL(float speed)
{
	int8_t stateL=0,stateR=0;
	//DUB0   DUC3
	car.drive = Left;
	
	while(1)
	{
		if(stateR==0)
		{if(DUB0==1)
	{
		HAL_Delay(5);
		if(DUB0==1)
	{
	car.speed_R = 0;
	stateR=1;}
	}
	else car.speed_R = speed;}
//==========================	
		if(stateL==0)
		{if(DUC3==1)
	{
		HAL_Delay(5);
		if(DUC3==1)
	{	
	car.speed_L = 0;
	stateL=1;}
	}
	else car.speed_L = speed;}
	
	if (stateL!=0&&stateR!=0)break;



}
}
void alignmentR(float speed)
{
	int8_t stateL=0,stateR=0;
	//DUB0   DUC3
	car.drive = Right;
	
	while(1)
	{
		if(stateR==0)
		{if(DUA2==1)
	{
		HAL_Delay(5);
		if(DUA2==1)
	{
	car.speed_R = 0;
	stateR=1;}
	}
	else car.speed_R = speed;}
//==========================	
	if(stateL==0)
	{if(DUA3==1)
	{
		HAL_Delay(5);
		if(DUA3==1)
	{	
	car.speed_L = 0;
	stateL=1;}
	}
	else car.speed_L = speed;}
	
	if (stateL!=0&&stateR!=0)break;



}


}
void alignmentF(float speed)//2激光对齐
{
	int8_t stateL=0,stateR=0;
	//DUB0   DUC3
	car.drive = Forward;

		
		while(1)
	{
	if(stateR==0)
	{
	if(DUI2==1)
	{
//		HAL_Delay(2);
		if(DUI2==1)
	{
	car.speed_R = 0;
	stateR=1;}
	}
	else car.speed_R = speed;
 }else car.speed_R = 0;
//==========================	
	if(stateL==0)
	{
		if(DUI7==1)
	{
//		HAL_Delay(2);
		if(DUI7==1)
	{	
	car.speed_L = 0;
	stateL=1;}
	}
	else car.speed_L = speed;
}else car.speed_L = 0;
	if (stateL!=0&&stateR!=0)break;
	
 }


}
void Back_duiqi(void )
{
		uint8_t a=0,b=0;
 Go_( 1000,Back );//不寻迹走
	while(1)
	{
	a=	DUH12;
		if(a==1)b=1;
		if(a==0&&b==1)break;

	}
	Stop();
	

}

void alignmentB(float speed)//两点对一条线，一点碰到线马上停
{
	int8_t stateL=0,stateR=0;
	//DUB0   DUC3
	car.drive = Back;

		
		while(1)
	{
	if(stateR==0)
	{
	if(DUI7==1)
	{
//		HAL_Delay(2);
		if(DUI7==1)
	{
	car.speed_R = 0;
	stateR=1;}
	}
	else car.speed_R = speed;
 }else car.speed_R = 0;
//==========================	
	if(stateL==0)
	{
		if(DUI2==1)
	{
//		HAL_Delay(2);
		if(DUI2==1)
	{	
	car.speed_L = 0;
	stateL=1;}
	}
	else car.speed_L = speed;
}else car.speed_L = 0;
	if (stateL!=0&&stateR!=0)break;
	
 }
	
 




}
void Catch_to_OD(float speed)//抓完后退到黑线后面
{
  alignmentB(speed);
	
	Turning_Laps(5,LMR_SPEED, Forward );
	alignmentB(1000);

}
void Catch_to_O(float speed)//抓完后退到黑线后面
{

		alignmentB(speed);
		Turning_Laps(5,LMR_SPEED, Forward );
		alignmentB(1000);


}
	void Fang_to_O(float speed)//抓完后退到黑线后面  区域二放东西
	{	

	alignmentB(speed);
	Turning_Laps(6,1000, Forward );
	alignmentB(1000);
	}
//	void Fang1_to_O(float speed)//不用了
//	{
//		
//		Turning_Laps(15,2000, Back );
//		alignmentB(speed);
//		Turning_Laps(5,1000, Forward );
//		alignmentB(speed);
//	
//	}
	
	void O_to_catchD(uint8_t LMR,float speed)//后激光定位
	{	
//		alignmentB(1000);
		int8_t  u=0,num=1;
	if(LMR==L||LMR==R)
	{
	while(num)
		{
				//TraceF_speed(speed);
				Go_( speed,Forward);//不巡线
			//TraceF_speed( speed);//不巡线
		if(DUH12==1)//LEFT3
		{
//			HAL_Delay(2);
			if(DUH12==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	}
	else if(LMR==M)
	{
		while(num)
		{
				//TraceF_speed(speed);
			Go_( speed,Forward);//不巡线
			//TraceF_speed1( speed);//不巡线
		if(DUH12==1)//LEFT3
		{
//			HAL_Delay(2);
			if(DUH12==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	
	}
		
//Turning_Laps(3,1000 ,Forward);
	Stop();

	
	}
void O_to_catch(uint8_t LMR,float speed)//入口参数L：数左边的线 R数右边的线 M：线在车正中间
																				/*从黑线后面往前走到能夹住物块的地方  */
{
	int8_t  u=0,num=1;

	if(LMR==L||LMR==R)
	{
	while(num)
		{
				//TraceF_speed(speed);
				Go_( speed,Forward);//不巡线
		
		if(DUH12==1)//LEFT3
		{
//			HAL_Delay(2);
			if(DUH12==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	}
	else if(LMR==M)
	{
		while(num)
		{
				//TraceF_speed(speed);
			Go_( speed,Forward);//不巡线
			//TraceF_speed1( speed);//不巡线
		if(DUH12==1)//LEFT3
		{
//			HAL_Delay(2);
			if(DUH12==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	
	}
		
//Turning_Laps(5,1000 ,Forward);
	Stop();


	/*
	int8_t  u=0,num=1;
		car.drive = Forward;
if(LMR==R)//=========================================
{
	while(num)
		{
				//TraceF_speed(speed);
				Go_( speed,Forward);//不巡线
		if(DUA3==1)//LEFT3
		{
			HAL_Delay(5);
			if(DUA3==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	}
else if(LMR==M)//=========================================
{
	while(num)
		{
				TraceF_speed(speed);
				
		if(DUB0==1)//LEFT3
		{
			HAL_Delay(5);
			if(DUB0==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;

		}
}
else if(LMR==L)//=========================================
{
	while(num)
		{
					Go_( speed,Forward);//不巡线
				
		if(DUB0==1)//LEFT3
		{
			HAL_Delay(5);
			if(DUB0==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}




}
Turning_Laps(13,alittle_speed,Forward);

	Stop();

*/
}
void O_to_Fang(uint8_t LMR,float speed)
{
	int8_t  u=0,num=1;
	
	if(LMR==L||LMR==R)
	{
	while(num)
		{
				//TraceF_speed(speed);
				Go_( speed,Forward);//不巡线
			//TraceF_speed( speed);//不巡线
		if(DUH12==1)//LEFT3
		{
//			HAL_Delay(2);
			if(DUH12==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	}
	else if(LMR==M)
	{
		while(num)
		{
				//TraceF_speed(speed);
			Go_( speed,Forward);//不巡线
			//TraceF_speed1( speed);
		if(DUH12==1)//LEFT3
		{
//			HAL_Delay(2);
			if(DUH12==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	
	}


	Stop();


}

void LMR_to_LMRD(uint8_t LMR,uint8_t Taget,uint8_t A)//将手中物块放入盘子，去目的地抓住物块
{
	if(A==0)	{Open(); }

	Catch_to_OD(min_speed);
	
if(LMR==R)//=========================车在右边======================
{
if(Taget==M)
{
	if(A!=0)	{TOP1(2);vTaskDelay(delay_tim);}

	
	Fang_state=A;
	
	GoL_Num(1,min_speed-500,1);//不寻迹数线  速度要慢
		
	while(Fang_state)vTaskDelay(3);
	
			if(A!=0)
		{
	High_up1(2);
	vTaskDelay(delay_tim);
		}
		
	O_to_catchD(M,min_speed);
}
else if(Taget==L)
{
	if(A!=0)	{TOP1(2);vTaskDelay(delay_tim);}
	Fang_state=A;
	
	GoL_Num(2,min_speed-500,0);//不寻迹数线  速度要慢
		while(Fang_state)vTaskDelay(3);
	
			if(A!=0)
		{
	High_up1(2);
	vTaskDelay(delay_tim);
		}
	O_to_catchD(L,min_speed);
}


}
else if(LMR==M)//=========================车在中间======================
{
	if(Taget==R)
{
	if(A!=0)	{TOP1(2);vTaskDelay(delay_tim);	Fang_state=A;}//mei没有抬升

	
	GoR_Num(1,min_speed-500,0);//不寻迹数线  速度要慢

	while(Fang_state)vTaskDelay(3);
		if(A!=0)
		{
	High_up1(2);
	vTaskDelay(delay_tim);
		}
		
	O_to_catchD(R,min_speed);
	
}
else if(Taget==L)
{
	if(A!=0)	{TOP1(2);vTaskDelay(delay_tim);		Fang_state=A;}

	GoL_Num(1,min_speed-500,0);//不寻迹数线  速度要慢
	while(Fang_state)vTaskDelay(3);
		if(A!=0)
		{
	High_up1(2);
	vTaskDelay(delay_tim);
		}
	O_to_catchD(L,min_speed);
	
}
}
else if(LMR==L)//=========================车在左边======================
{
if(Taget==M)
{	
	if(A!=0)	{TOP1(2);vTaskDelay(delay_tim);	Fang_state=A;}

	
	GoR_Num(1,min_speed-500,1);//不寻迹数线  速度要慢
		while(Fang_state)vTaskDelay(3);
		if(A!=0)
		{
	High_up1(2);
	vTaskDelay(delay_tim);
		}
	O_to_catchD(M,min_speed);
	
}
else if(Taget==R)
{			if(A!=0)	{TOP1(2);vTaskDelay(delay_tim);Fang_state=A;}
	

	GoR_Num(2,min_speed-500,0);//不寻迹数线  速度要慢
	while(Fang_state)vTaskDelay(3);

		if(A!=0)
		{
	High_up1(2);
	vTaskDelay(delay_tim);
		}
//	
	O_to_catchD(M,min_speed);
}
}
	Close(close_speed);



}










//==========================================
void LMR_to_LMR(uint8_t LMR,uint8_t Taget)//==================================区域一
{
if(LMR==R)//=========================车在右边======================
{
if(Taget==M)
{
	Catch_to_O(min_speed);	
	GoL_Num(1,min_speed-500,1);//不寻迹数线  速度要慢
	O_to_catch(M,up_speed);
}
else if(Taget==L)
{
	Catch_to_O(min_speed);	
	GoL_Num(2,min_speed-500,0);//不寻迹数线  速度要慢
	O_to_catch(L,up_speed);
}


}
else if(LMR==M)//=========================车在中间======================
{
	if(Taget==R)
{
	Catch_to_O(min_speed);	
	GoR_Num(1,min_speed-500,0);//不寻迹数线  速度要慢
	O_to_catch(R,up_speed);
	
}
else if(Taget==L)
{
	Catch_to_O(min_speed);	
	GoL_Num(1,min_speed-500,0);//不寻迹数线  速度要慢
	O_to_catch(L,up_speed);
	
}
}
else if(LMR==L)//=========================车在左边======================
{
if(Taget==M)
{
	Catch_to_O(min_speed);	
	GoR_Num(1,min_speed-500,1);//不寻迹数线  速度要慢
	O_to_catch(M,up_speed);
	
}
else if(Taget==R)
{
	Catch_to_O(min_speed);	
	GoR_Num(2,min_speed-500,0);//不寻迹数线  速度要慢
	O_to_catch(M,up_speed);
}
}


}
//======================================




void LMR_to_LMR_fast(uint8_t LMR,uint8_t Taget)//快速抓回物块
{
if(LMR==R)//=========================车在右边======================
{
if(Taget==M)
{
	GoL_Num(1,LMR_SPEED1,1);//不寻迹数线  速度要慢
	if(two_3_1==0)
//		alignmentB(LMR_SPEED1);
	Fang_to_O(LMR_SPEED1);	//前进
	
	O_to_Fang(M,LMR_SPEED1);//houtu后退
}
else if(Taget==L)
{
	
	
	GoL_Num(2,LMR_SPEED1,0);//不寻迹数线  速度要慢
		if(two_3_1==0)
//	alignmentB(LMR_SPEED1);	
			Fang_to_O(LMR_SPEED1);	//前进
		
	O_to_Fang(L,LMR_SPEED1);	
	
}


}
else if(LMR==M)//=========================车在中间======================
{
	if(Taget==R)
{GoR_Num(1,LMR_SPEED1,0);//不寻迹数线  速度要慢
	if(two_3_1==0)
//	alignmentB(LMR_SPEED1);	
		Fang_to_O(LMR_SPEED1);	//前进
	
	O_to_Fang(R,LMR_SPEED1);	
}
else if(Taget==L)
{
	GoL_Num(1,LMR_SPEED1,0);//不寻迹数线  速度要慢
		if(two_3_1==0)
//	alignmentB(LMR_SPEED1);	
	Fang_to_O(LMR_SPEED1);	//前进
	
	O_to_Fang(L,LMR_SPEED1);	
}
}
else if(LMR==L)//=========================车在左边======================
{
if(Taget==M)
{	
	GoR_Num(1,LMR_SPEED1,1);//不寻迹数线  速度要慢
		if(two_3_1==0)
//	alignmentB(LMR_SPEED1);	
	Fang_to_O(LMR_SPEED1);	//前进
	O_to_Fang(M,LMR_SPEED1);
}
else if(Taget==R)
{	
	GoR_Num(2,LMR_SPEED1,0);//不寻迹数线  速度要慢
		if(two_3_1==0)
//	alignmentB(LMR_SPEED1);	
	Fang_to_O(LMR_SPEED1);	//前进
	O_to_Fang(M,LMR_SPEED1);	
}
}

Turning_Laps(7 ,1000 ,Back );//后退点防止打到五块
	two_3_1=0;
}






 void Back_down(float speed)//dingdiaj定点
{

		while(1)
		{
					Go_(speed,Back);			
		if(DUI0 ==1)//LEFT3
		{
			break;
		}
		}
		
		Stop();
		
}
void Duidian2(void )//对准
{
	uint8_t a=0,b=0;
 Go_( 1000,Forward );//不寻迹走
	while(1)
	{
	a=	DUI0;
		if(a==1)b=1;
		if(a==0&&b==1)break;

	}
	Stop();

}
void Duidian(void )
{	
	alignmentF(CPQ_SPEED1);
//	Turning_Laps(5 ,800 ,Back );
//	alignmentF(CPQ_SPEED1);
	Back_down(CPQ_SPEED2);
}
void Duidian1(void )
{	
	alignmentF(CPQ_SPEED1);
//	Turning_Laps(5 ,800 ,Back );
//	alignmentF(CPQ_SPEED1);
	Back_down(CPQ_SPEED2);
}

void LMR_to_LMRM(uint8_t LMR,uint8_t Taget)//LMR车当前位置  Taget目的地位置
{
	
if(LMR==R)//=========================车在右边====================== Turning_Laps(uint16_t num,float speed,car_Typedef fx);//编码器行走前后左右   编码器旋转   或  斜走
{
if(Taget==M)
{
	GoL_Num3(1 ,CPQ_SPEED1 ,1 );//不寻迹数线  速度要慢

	
}
else if(Taget==L)
{
		GoL_Num3(2 ,CPQ_SPEED1 ,0 );//不寻迹数线  速度要慢

}


}
else if(LMR==M)//=========================车在中间======================
{
	if(Taget==R)
{	
	GoR_Num3(1 ,CPQ_SPEED1 ,0 );//不寻迹数线  速度要慢
}
else if(Taget==L)
{
	GoL_Num3(1 ,CPQ_SPEED1 ,0 );//不寻迹数线  速度要慢
}
}
else if(LMR==L)//=========================车在左边======================
{
if(Taget==M)
{	
	GoR_Num3(1 ,CPQ_SPEED1 ,1 );//不寻迹数线  速度要慢
}
else if(Taget==R)
{	
	GoR_Num3(2,CPQ_SPEED1 ,0 );//不寻迹数线  速度要慢
}
}

if(updowm==1)Duidian1();


else Duidian();
}













void LMR_to_LMR1(uint8_t LMR,uint8_t Taget)//LMR车当前位置  Taget目的地位置//=============================区域二
{
if(LMR==R)//=========================车在右边======================
{
if(Taget==M)
{
	GoL_Num(1,LMR_SPEED,1);//不寻迹数线  速度要慢
	Fang_to_O(LMR_SPEED);	//前进
	O_to_Fang(M,LMR_SPEED);//houtu后退
}
else if(Taget==L)
{
	
	GoL_Num(2,LMR_SPEED,0);//不寻迹数线  速度要慢
	Fang_to_O(LMR_SPEED);	
	O_to_Fang(L,LMR_SPEED);
	
}


}
else if(LMR==M)//=========================车在中间======================
{
	if(Taget==R)
{	
	GoR_Num(1,LMR_SPEED,0);//不寻迹数线  速度要慢
	Fang_to_O(LMR_SPEED);	

	O_to_Fang(R,LMR_SPEED);
}
else if(Taget==L)
{
	GoL_Num(1,LMR_SPEED,0);//不寻迹数线  速度要慢
	Fang_to_O(LMR_SPEED);	

	
	O_to_Fang(L,LMR_SPEED);
}
}
else if(LMR==L)//=========================车在左边======================
{
if(Taget==M)
{	
	GoR_Num(1,LMR_SPEED,1);//不寻迹数线  速度要慢
	Fang_to_O(LMR_SPEED);	

	O_to_Fang(M,LMR_SPEED);
}
else if(Taget==R)
{	
	GoR_Num(2,LMR_SPEED,0);//不寻迹数线  速度要慢
	Fang_to_O(LMR_SPEED);	

	O_to_Fang(M,LMR_SPEED);
	
}
}
}
//===================================================成品区
//void LMR_to_LMR2(uint8_t LMR,uint8_t Taget)//LMR车当前位置  Taget目的地位置
//{
//if(LMR==R)//=========================车在右边======================
//{
//if(Taget==M)
//{
//	if(updowm==1){
//		GoL_Num(1,LMR_SPEED,1);//不寻迹数线  速度要慢
//		
//	O_to_Fang1(M,LMR_SPEED);//houtu后退
//	}
//	else
//	{
//		

//	GoL_Num(1,LMR_SPEED,1);//不寻迹数线  速度要慢
//	alignmentB(LMR_SPEED);	//
//	O_to_Fang1(M,LMR_SPEED);//houtu后退
//	}
//}
//else if(Taget==L)
//{
//		if(updowm==1){
//				GoL_Num(2,LMR_SPEED,0);//不寻迹数线  速度要慢
//				O_to_Fang1(L,LMR_SPEED);
//		}
//	
//	
//	else{
//		GoL_Num(2,LMR_SPEED,0);//不寻迹数线  速度要慢
//	Fang1_to_O(LMR_SPEED);	

//	O_to_Fang1(L,LMR_SPEED);}
//}


//}
//else if(LMR==M)//=========================车在中间======================
//{
//	if(Taget==R)
//{
//			if(updowm==1){
//			
//	GoR_Num(1,LMR_SPEED,0);//不寻迹数线  速度要慢
//	O_to_Fang1(R,LMR_SPEED);}
//			else
//			{Fang1_to_O(LMR_SPEED);	
//	GoR_Num(1,LMR_SPEED,0);//不寻迹数线  速度要慢
//	O_to_Fang1(R,LMR_SPEED);}
//}
//else if(Taget==L)
//{
//			if(updowm==1){	GoL_Num(1,LMR_SPEED,0);//不寻迹数线  速度要慢
//	O_to_Fang1(L,LMR_SPEED);}
//			
//			else
//			{
//	GoL_Num(1,LMR_SPEED,0);//不寻迹数线  速度要慢
//	Fang1_to_O(LMR_SPEED);	
//	
//	O_to_Fang1(L,LMR_SPEED);}
//}
//}
//else if(LMR==L)//=========================车在左边======================
//{
//if(Taget==M)
//{
//			if(updowm==1){	
//	
//	GoR_Num(1,LMR_SPEED,1);//不寻迹数线  速度要慢
//	O_to_Fang1(M,LMR_SPEED);}
//			else
//			{
//	alignmentB(LMR_SPEED);	
//	GoR_Num(1,LMR_SPEED,1);//不寻迹数线  速度要慢
//	O_to_Fang1(M,LMR_SPEED);}
//}
//else if(Taget==R)
//{	
//			if(updowm==1){	GoR_Num(2,LMR_SPEED,0);//不寻迹数线  速度要慢
//	O_to_Fang1(M,LMR_SPEED);}
//			else{
//	GoR_Num(2,LMR_SPEED,0);//不寻迹数线  速度要慢
//	Fang1_to_O(LMR_SPEED);	

//	O_to_Fang1(M,LMR_SPEED);}
//}
//}

//}
void O_to_Fang1(uint8_t LMR,float speed)
{

	int8_t  u=0,num=1;
	Turning_Laps(15 ,speed,Forward );
	if(LMR==L||LMR==R)
	{
	while(num)
		{
				//TraceF_speed(speed);
				Go_( speed,Forward);//不巡线
			//TraceF_speed( speed);//不巡线
		if(DUI0==1)//LEFT3
		{
//			HAL_Delay(2);
			if(DUI0==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	}
	else if(LMR==M)
	{
		while(num)
		{
				//TraceF_speed(speed);
				//Go_( speed,Forward);//不巡线
		TraceF_speed1( speed);//不巡线
		if(DUI0==1)//LEFT3
		{
//			HAL_Delay(2);
			if(DUI0==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;



		}
	
	}
		

	Stop();


}

