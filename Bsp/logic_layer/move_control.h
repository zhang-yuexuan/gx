#ifndef __MOVE_CONTROL_H
#define __MOVE_CONTROL_H

#include "main.h"

#endif


void Begin(void);
void End(void);
void Move_Left(uint8_t cell);
void Move_Right(uint8_t cell);
void Move_Forward(void);
void Move_Back(uint8_t Direction);
void Move_Foreward_Close(void);
void Left_Aligment(void);
void Right_Aligment(void);
void Algment(uint8_t times);
void Go_To_Rough_Area(void);
void Move_Left_Rough(uint8_t position);

void Circle_Saver(uint8_t position, int speed);
void Open_Catcher(void);
void Close_Catcher(void);
void Put_Block(void);
void Get_Block(void);
void Stand_Block(void);
void Highter_Top(void);
void Highter_Middle(void);
void Highter_Buttop(void);

uint8_t Catch_First(void);
uint8_t Catch_Second(uint8_t Position);
uint8_t Catch_Third(uint8_t Position);

uint8_t Catch_Second_First(void);
uint8_t Catch_Second_Second(uint8_t Position);
uint8_t Catch_Second_Third(uint8_t Position);

void Move_Right_Rough(uint8_t position);
void Move_Left_Rough(uint8_t position);

void Put_Rough_First_Block(void);
void Put_Rough_Second_Block(void);
void Put_Rough_Third_Block(void);

void Up_Put_Rough_First_Block(void);
void Up_Put_Rough_Second_Block(void);
void Up_Put_Rough_Third_Block(void);

void Get_Rough_First_Block(void);
void Get_Rough_Second_Block(void);
void Get_Rough_Third_Block(void);

void Up_Get_Rough_First_Block(void);
void Up_Get_Rough_Second_Block(void);
void Up_Get_Rough_Third_Block(void);


void Go_to_Last(void);
void Go_To_Saver(void);

void Put_Last_First_Block(void);
void Put_Last_Second_Block(void);
void Put_Last_Third_Block(void);

void Up_Put_Last_First_Block(void);
void Up_Put_Last_Second_Block(void);
void Up_Put_Last_Third_Block(void);


void Down_Put_Last_First_Block(void);
void Down_Put_Last_Second_Block(void);
void Down_Put_Last_Third_Block(void);
