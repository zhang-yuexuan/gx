#ifndef __LINE_TRACK_H
#define __LINE_TRACK_H

#endif

#include "main.h"
#include "maintask2.h"
#define L 1
#define M 2
#define R 3


//=============================直接运动函数==============
void Turning(float speed,car_Typedef fx);//旋转
void Go_(float speed,car_Typedef fx);//不寻迹走

//=============================寻迹相关函数==============
void TraceF_speed1(uint16_t speed);//F寻迹

void TraceF_speed(uint16_t speed);//F寻迹
void TraceB_speed(uint16_t speed);
void TraceL_speed(uint16_t speed);
void TraceR_speed(uint16_t speed);


void Stop(void);//停车


//=============================编码器行走==============
extern void Turning_Laps(uint16_t num,float speed,car_Typedef fx);//编码器行走前后左右   编码器旋转   或  斜走
extern void Turning_Laps_T(uint16_t num,float speed,car_Typedef fx);//


//=============================数线相关函数==============
void TraceF_Num(uint16_t num,float speed);//寻迹数线
void TraceB_Num(uint16_t num,float speed);//寻迹数线

void TraceL_Num(uint16_t num,float speed,uint8_t mode);//左数线 mode：0 停在空格上  1 线上
void TraceR_Num(uint16_t num,float speed,uint8_t mode);//寻迹数线

void TraceL_FAST_Num(uint16_t num,float speed);//       左寻迹激光
void TraceF_FAST_Num(uint16_t num,float speed);//前寻迹数线  激光
void TraceL_Num_Ljg(uint16_t num,float speed,uint8_t mode);//左右激光数线    左寻迹
void TraceL_Num_Fjg(uint16_t num,float speed,uint8_t mode);//前激光数线    左寻迹
void Trace_L_Num_Bjg(uint16_t num,float speed );//后激光数线  左走
void Trace_L_Num_30(uint16_t num,float speed );//后激光数线  左走

void GoB_Num_Bjg(uint16_t num,float speed );//后激光数线  后走


void GoB_Num(uint16_t num,float speed );//后数线不寻迹  3-1  1-2    2-3


//==========
void GoL_Num(uint16_t num,float speed,uint8_t mode);//区域一和二  0-1  3-1   3-0
void GoR_Num(uint16_t num,float speed,uint8_t mode);//区域一前普通灰度数线    区域二后四个灰度数线


void GoL_Num2(uint16_t num,float speed,uint8_t mode);//1-2  2-3 右后普通灰度  


void GoL_Num3(uint16_t num,float speed,uint8_t mode);//成品区慢速
void GoR_Num3(uint16_t num,float speed,uint8_t mode);//
//==========



void Turn_Num(uint16_t num,float speed,car_Typedef fx,car_Typedef fx1);//参数： num  线的数量  speed 速度    fx 旋转方向   fx1 旋转后哪个方向卡住线



//======================对齐函数=====================================
void alignmentL(float speed);//对齐左边黑线
void alignmentR(float speed);//对齐右边  //两点对一条线，一点碰到线马上停
void alignmentB(float speed);//2激光对齐

void alignmentF(float speed);//2激光对齐


//======================货架区=====================================（在O区域可以定好三个位置直接去抓物块）

void Catch_to_O(float speed);//抓完后退到黑线后面
void O_to_catch(uint8_t LMR,float speed);//后激光定位
void LMR_to_LMR(uint8_t LMR,uint8_t Taget);//LMR车当前位置  Taget目的地位置

//======================粗加工区=====================================
void Fang_to_O(float speed);//抓完后退到黑线后面
void O_to_Fang(uint8_t LMR,float speed);//后激光定位
void LMR_to_LMR1(uint8_t LMR,uint8_t Taget);//LMR车当前位置  Taget目的地位置

void LMR_to_LMR_fast(uint8_t LMR,uint8_t Taget);//快速抓回物块


void Back_duiqi(void );//

//======================成品区=====================================

void O_to_Fang1(uint8_t LMR,float speed);//放成品区，用前激光定位
//void LMR_to_LMR2(uint8_t LMR,uint8_t Taget);//没用到
//void Fang1_to_O(float speed);//抓完后退到黑线后面



//======================货架区下层=====================================（在O区域可以定好三个位置直接去抓物块）
void Catch_to_OD(float speed);//抓完后退到黑线后面
void O_to_catchD(uint8_t LMR,float speed);//后激光定位
void LMR_to_LMRD(uint8_t LMR,uint8_t Taget,uint8_t A);//LMR车当前位置  Taget目的地位置


//======================成品区码垛=====================================


void LMR_to_LMRM(uint8_t LMR,uint8_t Taget);//LMR车当前位置  Taget目的地位置
void Back_down(float speed);//dingdiaj定点

void Duidian(void );//对准
void Duidian1(void );//对准
void Duidian2(void );//对准













extern uint8_t r_plussRL ;
extern uint8_t l_plussRL ;
extern uint8_t r_plussM ;
extern uint8_t l_plussM ;
extern uint8_t Back_1;

extern uint16_t up_speed ;
extern uint8_t close_speed ;
extern uint8_t Fang_state ;
extern uint16_t High_speed3 ;
extern uint16_t bmq_speed ;
extern uint16_t down_speed ;
//暂时没用到
void TraceF_Turning_Laps(uint16_t num,float speed);//向f方向巡线（编码器）
void TraceB_Turning_Laps(uint16_t num,float speed);
void TraceL_Turning_Laps(uint16_t num,float speed);
void TraceR_Turning_Laps(uint16_t num,float speed);
void Direction_Initial(car_Typedef direction);
void GoL_Num1(uint16_t num,float speed,uint8_t mode);//无用
void GoR_Num1(uint16_t num,float speed,uint8_t mode);//无用

