#include "main.h"
#include "adc.h"
#include "can.h"
#include "dma.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"
#include "oled.h"
#include "bsp_can.h"
#include "pid.h"
#include "bsp_key.h"
#include "manu.h"
#include "stdio.h"
#include "stdlib.h"
#include "Gui.h"
#include "LCD.h"

extern uint8_t begin_flag;
extern RGB rgb;
extern RGB rgb_c;
extern uint8_t Color;
extern uint8_t Color_c;

void Manu_Interface(void){
	//Sensor();
	int slect=0;
	uint8_t direc;
	Display_Choise(0);
	while(1)
		{	
			while(!Get_Key())
				code();
			HAL_Delay(50);
			direc=Get_Key();
			HAL_Delay(50);
			while(Get_Key());
			HAL_Delay(50);
			switch(direc)
				{
					case 1:		slect = slect-1;  //按键向上
										if (slect < 0) slect = 5;
										Display_Choise(slect);
										break;
					case 2:		slect = slect+1;  //按键向下
										if (slect > 5) slect = 0;
										Display_Choise(slect);
										break;
					case 5:		Into_Detail(slect);//按下确定键
				}
		}
		
}

void Display_Choise(uint8_t ch){
	switch(ch)
				{
					case 0:		
						oled_showstring(0, 1, "->Begain WiFi&Coder ");
						oled_showstring(1, 1, "Sensor  ");
						oled_showstring(2, 1, "Distan  ");
						oled_showstring(3, 1, "Color  ");
						oled_showstring(4, 1, "Steer  ");
						oled_refresh_gram(); 
						break;
					case 1:		
						oled_showstring(0, 1, "Begain  WiFi&Coder  ");
						oled_showstring(1, 1, "->Sensor  ");
						oled_showstring(2, 1, "Distan  ");
						oled_showstring(3, 1, "Color  ");
						oled_showstring(4, 1, "Steer  ");
						oled_refresh_gram(); 
						break;
					case 2:		
						oled_showstring(0, 1, "Begain  WiFi&Coder  ");
						oled_showstring(1, 1, "Sensor  ");
						oled_showstring(2, 1, "->Distan  ");
						oled_showstring(3, 1, "Color  ");
						oled_showstring(4, 1, "Steer  ");
						oled_refresh_gram(); 
						break;
					case 3:		
						oled_showstring(0, 1, "Begain  WiFi&Coder  ");
						oled_showstring(1, 1, "Sensor  ");
						oled_showstring(2, 1, "Distan  ");
						oled_showstring(3, 1, "->Color  ");
						oled_showstring(4, 1, "Steer  ");
						oled_refresh_gram(); 
						break;
					case 4:		
						oled_showstring(0, 1, "Begain  WiFi&Coder  ");
						oled_showstring(1, 1, "Sensor  ");
						oled_showstring(2, 1, "Distan  ");
						oled_showstring(3, 1, "Color  ");
						oled_showstring(4, 1, "->Steer  ");
						oled_refresh_gram(); 
						break;
					case 5:		
						oled_showstring(0, 1, "Begain  ->WiFi&Coder");
						oled_showstring(1, 1, "Sensor  ");
						oled_showstring(2, 1, "Distan  ");
						oled_showstring(3, 1, "Color  ");
						oled_showstring(4, 1, "Steer  ");
						oled_refresh_gram(); 
						break;
				}
}

void Into_Detail(uint8_t ch){
	switch(ch)
				{
					case 0:		Begain();break;
					case 1:		Sensor();break;
					case 2:		Distance();break;
					case 3:		Get_Color();break;
					case 4:		Steering_engine();break;
					case 5:		WiFi();break;
				}
}


//-------------主程序----------------
// 			主程序启动标志位翻转       	
//			uint8_t begin_flag = 0;
//-----------------------------------
void Begain(void){
	begin_flag = 1;
}


void Sensor(void){
//-------------灰度显示--------------
//	ad值都存在ADC_Value里面
//	1：A面左模拟值	9 ：B面左侧
//	2：A面右模拟值	10：B面右侧
//	3：C面左模拟值	11：D面左侧
//	4：C面右模拟值	12：D面右侧
//-----------------------------------
/* ==============================================================
													定义小车方向
 * 往小车行驶方向看为正方向
	
								|=========F=====================											|
								|	 		  DU2		DU1 ↑	DUI9 		DUA4 |
								|	DUB0 		        ↑			     							DUA3     |		
								L-DUC2------      ↑-------  								DUA1						-R					
								|	DUB1 			      ↑												DUC1	    |
								|	DUC3			      ↑												DUA2	    |
								|				DUC0 DUC4	      ↑	DUC5	 DUA0        |
								|=========B=====================|
	
 ============================================================== */
oled_clear(Pen_Clear);
while(1)
	{
		oled_showstring(1, 10, "A");
		oled_showstring(3, 10, "C");
		oled_showstring(2, 7, "D");
		oled_showstring(2, 13, "B");
		oled_shownum(0,4,DU2,2,1); //A面靠B数字量  y   x   F
		oled_shownum(0,8,DU1,2,1); //A面靠B面模拟值
		oled_shownum(0,12,DUI9,2,1); //A面靠B面模拟值
		oled_shownum(0,16,DUA4,2,1); //A面靠D面模拟值
		
		oled_shownum(4,4,DUC0,2,1); //A面靠B数字量  y   x    B
		oled_shownum(4,8,DUC4,2,1); //A面靠B面模拟值 
		oled_shownum(4,12,DUC5,2,1); //A面靠B面模拟值
		oled_shownum(4,16,DUA0,2,1); //A面靠D面模拟值
		
		oled_shownum(1,1,DUB0,2,1); //A面靠B数字量  y   x    L
		oled_shownum(2,1,DUC2,2,1); //A面靠B面模拟值
		oled_shownum(3,1,DUB1,2,1); //A面靠B面模拟值
		oled_shownum(4,1,DUC3,2,1); //A面靠D面模拟值
		
		oled_shownum(1,20,DUA3,2,1); //A面靠B数字量  y   x    R
		oled_shownum(2,20,DUA1,2,1); //A面靠B面模拟值
		oled_shownum(3,20,DUC1,2,1); //A面靠B面模拟值
		oled_shownum(4,20,DUA2,2,1); //A面靠D面模拟值
		
		
//		oled_shownum(0,6,ADC_Value[0],2,4); //B面左模拟值
//		oled_shownum(0,11,ADC_Value[1],2,4); //B面右模拟值
//		oled_shownum(0,16,ADC_Value[2],2,4); //C面靠B数字量

//		oled_shownum(1,15,ADC_Value[4],2,4); //C面靠B面模拟值
//		oled_shownum(2,15,ADC_Value[3],2,4); //C面靠B面模拟值
//		oled_shownum(3,15,ADC_Value[8],2,4); //C面靠D面模拟值
//		oled_shownum(4,1,ADC_Value[10],2,4); //A面靠D数字量
//		oled_shownum(4,6,ADC_Value[9],2,4); //D面靠A面模拟值
//		oled_shownum(4,11,ADC_Value[6],2,4); //D面靠C面模拟值
//		oled_shownum(4,16,ADC_Value[7],2,4); //C面靠D数字量
//		oled_shownum(1,7,DU1,2,1); //A面靠D数字量
//		oled_shownum(1,13,DU2,2,1); //C面靠D数字量
		oled_refresh_gram();
		HAL_Delay(100);
	}
}


void Distance(void){
//-------------距离显示--------------
uint8_t strff[8];
oled_clear(Pen_Clear);
while(1)
	{
		oled_showstring(1, 3, (uint8_t *)"L:");
		oled_showstring(3, 3, (uint8_t *)"F:");
		oled_showstring(1, 12, (uint8_t *)"R:");
		oled_showstring(3, 12, (uint8_t *)"B:");
		sprintf((char *)strff,"%.3f",Pre_range); // P Value
		oled_showstring(3,6,strff);
		sprintf((char *)strff,"%.3f",Bak_range); // B Value
		oled_showstring(3,14,strff);
		sprintf((char *)strff,"%.3f",Left_range); // L Value
		oled_showstring(1,6,strff);
		sprintf((char *)strff,"%.3f",Right_range); // R Value
		oled_showstring(1,14,strff);
		oled_refresh_gram();
		HAL_Delay(50);
	}
}


void Steering_engine(void){
//-------------舵机调整--------------
	oled_clear(Pen_Clear);
	int s1,s2,s3,s4,derector;
	int ch = 0;
	s1 = 90;
	s2 = 60;
	s3 = 100;
	s4 = 55;
	oled_showstring(1, 1, (uint8_t *)"^^              ");
	oled_showstring(0, 1, (uint8_t *)"S1  S2    S3  S4");
	oled_shownum(2,1,s1,2,3); 
	oled_shownum(2,5,s2,2,3); 
	oled_shownum(2,10,s3,2,3);
	oled_shownum(2,14,s4,2,3); 
	oled_refresh_gram();
	while(1)
		{
			while(!Get_Key())
				{
					if(Color==0x01) 	    oled_showstring(3, 9, (uint8_t *)"RED  ");
					else if(Color==0x02) 	oled_showstring(3, 9, (uint8_t *)"GREEN");
					else if(Color==0x03) 	oled_showstring(3, 9, (uint8_t *)"BLUE ");
					oled_refresh_gram();
				}
			HAL_Delay(50);
			derector=Get_Key();
			switch(derector)
				{
					case 1:
						if(ch == 0) {s1++;if(s1>290) s1=290;}
						else if(ch == 1) {s2++;if(s2>295) s2=295;}
						else if(ch == 2) {s3++;if(s3>300) s3=300;}
						else {s4++;if(s4>305) s4=305;}
							break;
					case 2:
						if(ch == 0) {s1--;if(s1<50) s1=50;}
						else if(ch == 1) {s2--;if(s2<55) s2=55;}
						else if(ch == 2) {s3--;if(s3<55) s3=55;}
						else {s4--;if(s4<60) s4=60;}
							break;
					case 3:
						ch--;
						if(ch<0) ch=3;
							break;
					case 4:
						ch++;
						if(ch>3) ch=0;
							break;
				}	
			switch(ch)
				{
					case 0:
						oled_showstring(1, 1, (uint8_t *)"^^              ");
						oled_showstring(0, 1, (uint8_t *)"S1  S2    S3  S4");
						break;
					case 1:
						oled_showstring(1, 1, (uint8_t *)"    ^^          ");
						oled_showstring(0, 1, (uint8_t *)"S1  S2    S3  S4");
						break;
					case 2:
						oled_showstring(1, 1, (uint8_t *)"          ^^    ");
						oled_showstring(0, 1, (uint8_t *)"S1  S2    S3  S4");
						break;
					case 3:
						oled_showstring(1, 1, (uint8_t *)"              ^^");
						oled_showstring(0, 1, (uint8_t *)"S1  S2    S3  S4");
						break;
				}
			__HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_1, s1);  //修改pwm占空比
			__HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_2, s2);  //修改pwm占空比
			__HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_3, s3);  //修改pwm占空比
			__HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_4, s4);  //修改pwm占空比
				oled_shownum(2,1,s1,2,3); 
				oled_shownum(2,5,s2,2,3); 
				oled_shownum(2,10,s3,2,3);
				oled_shownum(2,14,s4,2,3); 
				if(Color==0x01) 	    oled_showstring(3, 9, (uint8_t *)"RED  ");
				else if(Color==0x02) 	oled_showstring(3, 9, (uint8_t *)"GREEN");
				else if(Color==0x03) 	oled_showstring(3, 9, (uint8_t *)"BLUE ");
			oled_refresh_gram();
		}
}

			
void Get_Color(void){
//-------------颜色显示------------
	oled_clear(Pen_Clear);
	while(1)
	{
		if(Color==0x01) 	    oled_showstring(1, 9, (uint8_t *)"RED  ");
		else if(Color==0x02) 	oled_showstring(1, 9, (uint8_t *)"GREEN");
		else if(Color==0x03) 	oled_showstring(1, 9, (uint8_t *)"BLUE ");
//		rgb_c
		oled_showstring(2, 1, (uint8_t *)"R:");
		oled_showstring(2, 5, (uint8_t *)"G:");
		oled_showstring(2, 10, (uint8_t *)"B:");
		oled_shownum(3,1,rgb_c.Red,2,3); 
		oled_shownum(3,5,rgb_c.Green,2,3); 
		oled_shownum(3,10,rgb_c.Blue,2,3); 
		oled_refresh_gram();
	}
}

void WiFi(void){
//-------------WiFi调试--------------
	oled_clear(Pen_Clear);
	while(1)
	{
		oled_shownum(2,10,UART2_RECV_BUF[0]-0X30,2,1);
		oled_shownum(2,11,UART2_RECV_BUF[1]-0X30,2,1);
		oled_shownum(2,12,UART2_RECV_BUF[2]-0X30,2,1);
		oled_shownum(3,10,UART2_RECV_BUF[4]-0X30,2,1);
		oled_shownum(3,11,UART2_RECV_BUF[5]-0X30,2,1);
		oled_shownum(3,12,UART2_RECV_BUF[6]-0X30,2,1);
		oled_refresh_gram();
	}
}


void code(void)
{
	if(UART2_RECV_BUF[0]=='1')
	Show_Str(0,0,BLACK,BLACK,"1",32,0);
	else if(UART2_RECV_BUF[0]=='2')
	Show_Str(0,0,BLACK,BLACK,"2",32,0);
	else if(UART2_RECV_BUF[0]=='3')
	Show_Str(0,0,BLACK,BLACK,"3",32,0);
	
	if(UART2_RECV_BUF[1]=='1')
	Show_Str(24,0,BLACK,BLACK,"1",32,0);
	else if(UART2_RECV_BUF[1]=='2')
	Show_Str(24,0,BLACK,BLACK,"2",32,0);
	else if(UART2_RECV_BUF[1]=='3')
	Show_Str(24,0,BLACK,BLACK,"3",32,0);
	
	if(UART2_RECV_BUF[2]=='1')
	Show_Str(48,0,BLACK,BLACK,"1",32,0);
	else if(UART2_RECV_BUF[2]=='2')
	Show_Str(48,0,BLACK,BLACK,"2",32,0);
	else if(UART2_RECV_BUF[2]=='3')
	Show_Str(48,0,BLACK,BLACK,"3",32,0);
	
	
	if(UART2_RECV_BUF[4]=='1')
	Show_Str(72,0,BLACK,BLACK,"1",32,0);
	else if(UART2_RECV_BUF[4]=='2')
	Show_Str(72,0,BLACK,BLACK,"2",32,0);
	else if(UART2_RECV_BUF[4]=='3')
	Show_Str(72,0,BLACK,BLACK,"3",32,0);
	
	if(UART2_RECV_BUF[5]=='1')
	Show_Str(96,0,BLACK,BLACK,"1",32,0);
	else if(UART2_RECV_BUF[5]=='2')
	Show_Str(96,0,BLACK,BLACK,"2",32,0);
	else if(UART2_RECV_BUF[5]=='3')
	Show_Str(96,0,BLACK,BLACK,"3",32,0);
	
	if(UART2_RECV_BUF[6]=='1')
	Show_Str(120,0,BLACK,BLACK,"1",32,0);
	else if(UART2_RECV_BUF[6]=='2')
	Show_Str(120,0,BLACK,BLACK,"2",32,0);
	else if(UART2_RECV_BUF[6]=='3')
	Show_Str(120,0,BLACK,BLACK,"3",32,0);
}

