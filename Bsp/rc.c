//#include "main.h"
#include "rc.h"
#include "Remote_Control.h"
#include "pid.h"
#include "bsp_can.h"
#include "math.h"
#include "main.h"
#include "tim.h"
/*
1
3
2
		左： 左右ch3   上下ch4           右：左右ch1   上下ch2
*/

uint8_t   go_mode=2;//di定义运动模式  0正常模式   1全向运动小陀螺模式
int left=0;
int right=0;
float mouseX=0;
float mouseY=0;
uint8_t state=0;
uint8_t Vz_mode=1;
uint8_t switch_L=0;
uint8_t switch_R=0;
uint16_t key_W=0;
uint16_t key_S=0;
uint16_t key_A=0;
uint16_t key_D=0;
uint16_t key_SHIFT=0;
uint16_t key_CTRL=0;
uint16_t key_Q=0;
uint16_t key_E=0;
uint16_t key_R=0;
uint16_t key_F=0;
uint16_t key_G=0;
uint16_t key_Z=0;
uint16_t key_X=0;
uint16_t key_C=0;
uint16_t key_V=0;
float X_plus=0;
float Y_plus=0;
float YAW_INIT=0;
float USAR_6020Angle=0;//----------------步兵顺时针角度增大
float USAR_6020Y_Angle=0;////6020Y_:4000 -  5500       4950
float Vx=0;
float Vy=0;
float Vz=0;//0-8000   -------------yaw顺时针数值减小
float Bchang_plus_yaw;//全向运动模式下云台6020初始化角度，需要经过yaw角的计算
float rotor_Angle=0;//已经旋转的角度
float X_KM_OUT;
float Y_KM_OUT;
///////////////////////////////////////////////////////////可更改的参数
uint8_t DeadBand=100;//底盘跟随的死区，太小容易震荡
float rotor_V=4000;//8000  遥控器速度最大值

float Bchang=700;//云台补偿值（矫正角度）    没有陀螺仪的情况下，炮口朝前时6020返回的角度
//float Bchang=2230;//云台补偿值（矫正角度）    没有陀螺仪的情况下，炮口朝前时6020返回的角度
float USAR_6020Y_Angle_init=2750;//
float USAR_6020Y_Angle_lowest=2750;//4100-5400
float USAR_6020Y_Angle_highest=6500;//4100-5400

float dipan_return_maxspeed=3000;
float _6020_X_init=0;//步兵开机时云台6020初始角度值
float X_speed=0.15f;//鼠标左右速度系数
float Y_speed=0.4f;//鼠标前后速度系数

uint16_t key_contrL=1000;//阈值速度，大于这个速度则缓冲加速
uint16_t SPEED_pluss=150;//ctrl shift 每次加速减速的值   
uint16_t SPEED_a=15;//加速度5
uint16_t SPEED=500;//初始速度        SPEED为当前速度，他会慢慢增加到Target_speed，而不会一下子冲出去
uint16_t Target_speed=500;//初始目标速度速度    0-8000   
float file_speed =-1500;//拨弹的速度
float file_speed_max =-1500;
float file_speed_min =-1000;

float Fire_speed=134;//发射速度 
float Fire_low_speed=140;//发射速度 
float Fire_Fast_speed=170;//发射速度 
///////////////////////////////////////////////////////////
//void Mode_switch(void)
//{
//	if(switch_L==1)//电脑操作模式   全部模式基于陀螺仪
//	{
//		
//		RC_control_computer();
//	}
//	
//		else if(switch_L==3)//电脑操作模式，全向运动   普通运动   不使用陀螺仪
//	{
//		RC_computer2();
//		go_mode=2;//切换到模式一的时候保持底盘不转
//	}
//	else if(switch_L==2)//遥控器测试模式     
//	{
//	  RC_control_remote();
//		go_mode=2;//切换到模式一的时候保持底盘不转
//	}

//}







void speed_to_Target_speed(void)
{
	/*ji缓冲加速
	if(Vx==0&&Vy==0)SPEED=0;//如果车停下，SPEED=0

	if(Target_speed>key_contrL)
	{
  if(key_W==1||key_S==1||key_A==1||key_D==1)
	{
		
		if(SPEED>Target_speed)SPEED=Target_speed;
		else 
		{   
			SPEED=SPEED+SPEED_a;
			HAL_Delay(5);
		}
   }
  }
	else SPEED=Target_speed;*/
	SPEED=Target_speed;
}

//void key_contr(void)//shift加速  Ctrl减速
//{
//	speed_to_Target_speed();
//			if(key_SHIFT==1) if(Target_speed<8000)    {     Target_speed= 	Target_speed+SPEED_pluss;	HAL_Delay(100); }  //加速  消抖
//			if(key_CTRL==1)  if(Target_speed>0)       {     Target_speed= 	Target_speed-SPEED_pluss; HAL_Delay(100); }  //减速
//			if(key_Z==1)Target_speed= 	0;  															  	//快捷速度：50 			
//			if(key_X==1)Target_speed= 	1000;  															  	//快捷速度：0  刹车
//			if(key_C==1)Target_speed= 	3000;														    	//快捷速度：2000  
//			if(key_V==1)Target_speed= 	6000;
//			if(key_R==1){
//				  go_mode=1;
//				  if(state==0){Vz_mode++;state=1;}
//					if(Vz_mode>3){Vz_mode=1;}		}														    	//模式一：小陀螺+全向运动
//			else {state=0;}
//			if(key_F==1){go_mode=0;	Vz_mode=0;		}												   //模式二：正常运动
//			
////			if(key_F==1){ Fire_speed=  Fire_low_speed;                        		}	
////			if(key_G==1){ Fire_speed=  Fire_Fast_speed;														}	//减速发射		
////			if(file_speed<file_speed_max)			{file_speed=file_speed_max;}
////			if(file_speed>file_speed_min)    {file_speed=file_speed_min;}
//			if(key_G==1)dancang_1kai_0guan(1);//弹仓开
//		  else        dancang_1kai_0guan(0);  //弹仓关
//}
//void RC_computer2(void)//
//{

////	Vz=0;////
////////////全向运动算法，陀螺仪要稳定，否则容易飘/////////////////////////////////////////	
//				YAW_INIT=(float)-(360-(moto_chassis[4].angle+Bchang)*360/8192);//遥控器控制YAW_INIT=(float)XYZ.Yaw-(moto_chassis[4].angle+Bchang)*360/8192;//枪口指向的方向
//				USAR_6020Angle=(float)(X_plus)+Bchang;     				
//				rotor_Angle=YAW_INIT;//  以YAW_INIT指向的方向为正前方	




//				Vx=cos(rotor_Angle/180*My_Pi)*(key_D*SPEED-key_A*SPEED)+sin(rotor_Angle/180*My_Pi)*(key_W*SPEED-key_S*SPEED);
//				Vy=cos(rotor_Angle/180*My_Pi)*(key_W*SPEED-key_S*SPEED)-sin(rotor_Angle/180*My_Pi)*(key_D*SPEED-key_A*SPEED); 
//		
//////////////////////////////////////////////////////////////////////////////		
//	
//			ch[0]= -Vy+Vx;
//			ch[1]= -Vy-Vx;
//			ch[2]=  Vy-Vx;
////			ch[3]=  Vy+Vx;
////			ch[4]=  USAR_6020Angle;//顺时针增大
////			ch[5]=  USAR_6020Y_Angle;//此处需要限位
////	
////}
////void RC_control_computer(void)
////{

////	/*
////	
////	小陀螺总体思路：
////	假设遥控器ch4通道往前推，也就是小车往前面跑
////	在这个情况下小车以vz=2000的速度逆时针旋转
////	假设转过30度
////	小车前进的方向变成沿着y偏x轴30度方向偏移，此时小车应该朝着y偏x30度方向移动
////	
////	rotor_Angle：小车底盘坐标系的角度
////	USAR_6020Angle：云台的角度
////	注意：6020旋转到-8192度时不会转到6020的0度位置，而是不停旋转
////	*/
////			if(go_mode==0){dipan_return_x(); }   //模式二：正常运动								E										
////	      else 
////					{
////						if(Vz_mode%1==0)Vz=0;
////						if(Vz_mode%2==0)Vz=2000;
////						if(Vz_mode%3==0)Vz=3000;
//////					if(switch_R==1)Vz=0;////模式一：小陀螺+全向运动     R
//////				  if(switch_R==3)Vz=2000;
//////					if(switch_R==2)Vz=3000;
////				   } 									  
////				
//////////////全向运动算法，陀螺仪要稳定，否则容易飘/////////////////////////////////////////		
////		//说明：陀螺仪逆时针增大角度    6020逆时针减小角度（倒着装）				
////					 /*
////					 rotor_Angle值为0时，车子朝着前方走  为90时，车子向左平移
////					 USAR_6020Angle值减小时，云台顺时针转   反之逆时针转
////					 */
////					 			 //刚开机时，此处6020的角度为Bchang，因为Bchang_plus_yaw里面有个-yaw和yaw抵消了，X_plus也等于0
////				USAR_6020Angle=(float)X_plus+((XYZ.Yaw)*8191/360)+Bchang_plus_yaw;//刚开机时，此处6020的角度为Bchang，因为Bchang_plus_yaw里面有个-yaw和yaw抵消了
////      		// 这个参数跟6020装配有关，记得把6020电源的接口朝着外面装，然后打开freertos.c注释掉任务4的dianji_set_speed函数，烧录进去，上电，debug观察枪口朝前时
////		      //moto_chassis[4].angle的数值，此时moto_chassis[4].angle的值就是Bchang的值，在上面更改参数。
////					 
////					 rotor_Angle=(moto_chassis[4].angle-Bchang)*360/8192;//开机时由于装配导致的补偿角度就可以通过减去Bchang值得到0，所以此时底盘朝前	

////				Vx=cos(rotor_Angle/180*My_Pi)*(key_D*SPEED-key_A*SPEED)+sin(rotor_Angle/180*My_Pi)*(key_W*SPEED-key_S*SPEED);
////				Vy=cos(rotor_Angle/180*My_Pi)*(key_W*SPEED-key_S*SPEED)-sin(rotor_Angle/180*My_Pi)*(key_D*SPEED-key_A*SPEED); 
////////////////////////////////////////////////////////////////////////////////				


////			ch[0]= -Vy+Vx+Vz;
////			ch[1]= -Vy-Vx+Vz;
////			ch[2]=  Vy-Vx+Vz;
////			ch[3]=  Vy+Vx+Vz;
////			ch[4]=  USAR_6020Angle;//顺时针增大
////			ch[5]=  USAR_6020Y_Angle;//此处需要限位
////		
////			
////			if(ch[4]>0)ch[4]=ch[4]-8191;//限制ch4数值防止溢出
////		  if(ch[4]<=-8191) ch[4]=ch[4]+8191;
//////////////////////////////////////////////////////////////////////////////	

////}
////void mouse_XY(void)
////{
////	if     (X_plus>8192)X_plus=X_plus-8192; //鼠标x轴移动  限值
////	else if(X_plus<-8192)X_plus=X_plus+8192;//鼠标x轴移动  限值
////		

////			   USAR_6020Y_Angle=(float)(USAR_6020Y_Angle_init-Y_plus);
////	

////	
//////					   Y_plus=Y_plus-Y_KM_OUT*Y_speed;//修改系数可提高y轴速度
//////						 X_plus=X_plus+X_KM_OUT*X_speed;//修改系数可提高x轴速度
////				 X_plus=X_plus+mouseX*X_speed;//修改系数可提高x轴速度
////			   Y_plus=Y_plus-mouseY*Y_speed;//修改系数可提高y轴速度
////				 if(switch_R==3&&STA_USART6==1)
////				 {
////					 X_plus=X_plus+OUTput_X;
////				 }
////	if     (USAR_6020Y_Angle>USAR_6020Y_Angle_highest)
////					{USAR_6020Y_Angle=USAR_6020Y_Angle_highest;Y_plus=USAR_6020Y_Angle_init-USAR_6020Y_Angle;}//
////	else if(USAR_6020Y_Angle<USAR_6020Y_Angle_lowest)
////					{USAR_6020Y_Angle=USAR_6020Y_Angle_lowest;Y_plus=USAR_6020Y_Angle_init-USAR_6020Y_Angle;}//

////  
////	if(right==1)//右键摩擦轮转起来
////	{
//////		Fric_Fire(Fire_speed);
////	}
////	else
////		{//松开停止发射
//////	Fric_OFF();
////  }
////	
////	////////
////	 if(left==1)//左键开始拨弹发射
////	{
////	ch[6]=file_speed;
////	}
////	else{
////	ch[6]=0;
////  }
////	

////}
//void dianji_set_speed(void)
//{
//	///////////////////电机pid/////////////////////////////////////////////
//	/**********************************************************************************
//	1
//	3
//	2
//     q   w e r
//       a s d f g	
//	     z x c v
//sh	
//ct
//   *          V    C    X	  Z    G    F    R   E   Q  CTRL  SHIFT  D   A   S   W
//************************************************************************************/
//	  switch_L=   remote_control.switch_left;  		//遥控器左按键（从上到下：1,3，2）
//		switch_R=   remote_control.switch_right; 		//遥控器有按键（从上到下：1,3，2）
//		left=       remote_control.mouse.press_left;//发射
//		right=      remote_control.mouse.press_right;//摩擦轮（先摩擦轮开启，然后左键点射，不足：如果忘记按右键只按左键会损坏发射结构）
//		mouseX=     remote_control.mouse.x;//6020       
//		mouseY =    remote_control.mouse.y;//6020Y_     
//	  key_W =    (remote_control.keyBoard.key_code>>0)&0x0001;//前
//	  key_S =    (remote_control.keyBoard.key_code>>1)&0x0001;//后
//	  key_A =    (remote_control.keyBoard.key_code>>2)&0x0001;//左
//	  key_D =    (remote_control.keyBoard.key_code>>3)&0x0001;//右
//	  key_SHIFT =(remote_control.keyBoard.key_code>>4)&0x0001;//加速
//	  key_CTRL = (remote_control.keyBoard.key_code>>5)&0x0001;//减速
//		key_Q =    (remote_control.keyBoard.key_code>>6)&0x0001;//开关弹仓
//	  key_E =    (remote_control.keyBoard.key_code>>7)&0x0001;//模式二：底盘跟随模式
//		key_R =    (remote_control.keyBoard.key_code>>8)&0x0001;//模式一：小陀螺+全向运动 switch_R：:上：Vz=0  中：Vz=2000   下：Vz=3000
//		key_F = 	 (remote_control.keyBoard.key_code>>9)&0x0001;//小射速
//		key_G =    (remote_control.keyBoard.key_code>>10)&0x0001;//大射速
//		key_Z =    (remote_control.keyBoard.key_code>>11)&0x0001;//快捷速度：0
//		key_X =    (remote_control.keyBoard.key_code>>12)&0x0001;//快捷速度：1000  
//		key_C =    (remote_control.keyBoard.key_code>>13)&0x0001;//快捷速度：3000 
//		key_V =    (remote_control.keyBoard.key_code>>14)&0x0001;//快捷速度：6000 
//				//四个电机3506
//		
//}











//void dianji_return_y(void)//开机时6020和6020Y_能够缓慢复位（未完成） 
//{

//	
//	
//		ch[4]=Bchang;   //步兵炮口朝着前方时6020的补偿值
//	  ch[5]=USAR_6020Y_Angle_init;//6020Y_平行地面时的初始值    6020Y_有效值在4100到5100之间
//	 _6020_X_init=-((XYZ.Yaw)*8191/360);
//		Bchang_plus_yaw=Bchang+_6020_X_init;
//}




//void  start_on()
//{

//}

//void switch_power(void)
//{
////	if(key_V==1)  HAL_GPIO_WritePin(GPIOH, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5, GPIO_PIN_RESET); // switch on 24v power;
////	else          HAL_GPIO_WritePin(GPIOH, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5, GPIO_PIN_SET); // switch on 24v power

//}

//void RC_control_remote(void)
//{
//			Vz=0;

//				USAR_6020Angle=Bchang+remote_control.ch1*4096/660;

//				USAR_6020Y_Angle=USAR_6020Y_Angle_init-remote_control.ch2*4096/660;
//									

//			ch[0]= -remote_control.ch4*rotor_V/660+remote_control.ch3*rotor_V/660;
//			ch[1]= -remote_control.ch4*rotor_V/660-remote_control.ch3*rotor_V/660;
//			ch[2]=  remote_control.ch4*rotor_V/660-remote_control.ch3*rotor_V/660;
//			ch[3]=  remote_control.ch4*rotor_V/660+remote_control.ch3*rotor_V/660;
//			ch[4]=USAR_6020Angle;
//			ch[5]=USAR_6020Y_Angle;
//}





