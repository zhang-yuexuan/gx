/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#define LIMIT_MIN_MAX(x,min,max) (x) = (((x)<=(min))?(min):(((x)>=(max))?(max):(x)))
#define DU1  	  HAL_GPIO_ReadPin(GPIOI,GPIO_PIN_5)//读取电平
#define DU2 	  HAL_GPIO_ReadPin(GPIOI,GPIO_PIN_6)//读取电平
	
#define DUA0  	  HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0)//读取电平

//#define DUA3 	  HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_3)//读取电平	
//#define DUA1 	  HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_1)//读取电平	
//#define DUC1 	  HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_1)//读取电平	
//#define DUA2  	  HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_2)//读取电平

#define DUA3 	  HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_2)//读取电平	
#define DUA1 	 HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_1)//读取电平	
#define DUC1 	  HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_1)//读取电平	
#define DUA2  	  HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_3)//读取电平


#define DUA4 	  HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_4)//读取电平	
		
#define DUB0 	  HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)//读取电平	
#define DUB1 	  HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1)//读取电平	

#define DUC0  	  HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_0)//读取电平

#define DUC2  	  HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_2)//读取电平
#define DUC3 	  HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_3)//读取电平	
#define DUC4 	  HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_4)//读取电平	
#define DUC5 	  HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_5)//读取电平	

#define DUI9 	  HAL_GPIO_ReadPin(GPIOI,GPIO_PIN_9)//读取电平	

#define DUI2 	  HAL_GPIO_ReadPin(GPIOI,GPIO_PIN_2)//读取电平	R
#define DUI7 	  HAL_GPIO_ReadPin(GPIOI,GPIO_PIN_7)//读取电平	L  激光对齐

#define DUI0 	  HAL_GPIO_ReadPin(GPIOI,GPIO_PIN_0)//读取电平	  前激光
#define DUH11 	  HAL_GPIO_ReadPin(GPIOH,GPIO_PIN_11)//读取电平	  右后灰度 
#define DUH12 	  HAL_GPIO_ReadPin(GPIOH,GPIO_PIN_12)//读取电平	后激光
/* ==============================================================
													定义小车方向
 * 往小车行驶方向看为正方向
	
								|=========F=====================											|
								|	 		  DU2		DU1 ↑	DUI9 		DUA4 |
								|	DUB0 		        ↑			     							DUA3     |		
								L-DUC2------      ↑-------  								DUA1						-R					
								|	DUB1 			      ↑												DUC1	    |
								|	DUC3			      ↑												DUA2	    |
								|				DUC0 DUC4	      ↑	DUC5	 DUA0        |
		DUI7						|=========B=====================|   				DUI2
	
 ============================================================== */
typedef struct
{
    int  Red;
    int  Green;
    int  Blue;
} RGB;
extern uint16_t ADC_Value[15];
extern uint8_t Color;
extern uint8_t Color_c;
extern unsigned char UART2_RECV_BUF[20];//串口6 接受 发送缓存
extern unsigned char UART6_RECV_BUF[20];//串口6 接受 发送缓存
extern unsigned char UART7_RECV_BUF[128];//串口7 接受 发送缓存
extern float Pre_range;
extern float Bak_range;
extern float Left_range;
extern float Right_range;
extern float Pre_range1;
extern float Bak_range1;
extern float Left_range1;
extern float Right_range1;
extern uint32_t err;
extern uint32_t s;
extern uint16_t shang; 
extern uint16_t s1;
int Get_Key(void);
extern float xL;
//
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define RST_Pin GPIO_PIN_4
#define RST_GPIO_Port GPIOE
#define DC_Pin GPIO_PIN_5
#define DC_GPIO_Port GPIOE
#define OLED_DC_Pin GPIO_PIN_9
#define OLED_DC_GPIO_Port GPIOB
#define POWER_1_Pin GPIO_PIN_2
#define POWER_1_GPIO_Port GPIOH
#define POWER_2_Pin GPIO_PIN_3
#define POWER_2_GPIO_Port GPIOH
#define POWER_3_Pin GPIO_PIN_4
#define POWER_3_GPIO_Port GPIOH
#define POWER_4_Pin GPIO_PIN_5
#define POWER_4_GPIO_Port GPIOH
#define CS_Pin GPIO_PIN_10
#define CS_GPIO_Port GPIOF
#define KEY_Pin GPIO_PIN_2
#define KEY_GPIO_Port GPIOB
#define LED_RED_Pin GPIO_PIN_11
#define LED_RED_GPIO_Port GPIOE
#define LED_GREEN_Pin GPIO_PIN_14
#define LED_GREEN_GPIO_Port GPIOF
#define OLED_RST_Pin GPIO_PIN_10
#define OLED_RST_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

typedef enum
{
    Forward = 0x00,
    Back = 0x01,
    Left = 0x02,
    Right = 0x03,
    Turn_L = 0x04,
    Turn_R = 0x05,
    Sloping_L_F = 0x06,
    Sloping_R_F = 0x07,
    Sloping_L_B = 0x08,
    Sloping_R_B = 0x09,
	  Turn_L1 = 0x10,
	 Turn_L2 = 0x13,
	Forward1=0x11,
		Left1=0x12,
}car_Typedef;
typedef struct All_car_Typedef
{
  car_Typedef drive;
	uint16_t  speed_L;
	uint16_t  speed_R;
	
	uint16_t  speed_LB;
	uint16_t  speed_RB;
	uint16_t  speed_LF;
	uint16_t  speed_RF;
	
	uint16_t  speed_T;
	uint8_t   Distance_B;
	uint16_t  Distance;
	uint8_t   over;
}All_car_Typedef;

int Get_Key(void);
void Turning_Laps(uint16_t num,float speed,car_Typedef fx);
void Turning_Laps_T(uint16_t num,float speed,car_Typedef fx);
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
