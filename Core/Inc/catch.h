#ifndef __catch_H
#define __catch_H
#include "line_track.h"
#include "main.h"
#include "maintask.h"
#include "tim.h"

//#define Open_all 180
//#define Open_1 160
//#define Open_2 140

#define Open_all 180
#define Open_1 130
#define Open_2 120


#define Close_1 88//夹紧
#define Close_2 105//松一点


#define TOP 295//
#define Huojia_2 295//

#define Huojia_1 100//90

#define Ground 60//
#define Ground2 155//码垛

#define s3_0d  60  //0度水平
#define s3_180d  288//180度 上物理限位

#define s3_f5d  55//负5度
 #define s3_90d  175//90
 
 
#define Duopan_1 65//d舵盘1的位置
#define Duopan_2 160
#define Duopan_3 250

extern void Serbor(int s1,int s2,int s3,int s4,int speed);






//===================================爪子函数=======================
void Close(int speed);//夹紧
void Close_alit(int speed);//吊着物块
void Open(void);//准备抓东西
void Fang_slow3(void);//f慢慢放东西

void Fang_slow(void);//f慢慢放东西
void Fang_slow0(void);//f慢慢放东西
//===================================升降函数=======================
void High_groud(int speed);//地板
void High_groud2(int speed);//码垛二层
void High_up1(int speed);//货架一层
void High_up2(int speed);//货架二层  
void TOP1(int speed);//货架二层  
//===================================弯曲函数=======================
void _0D(int speed);
void _e15D(int speed);
void _90D(int speed);
void _180D(int speed);
//===================================舵盘函数=======================
void _DP1(int speed);
void _DP2(int speed);
void _DP3(int speed);

//===================================整合动作函数=======================

void Catch_up2_really(void);//准备抓二层的状态
void Catch_up1_really(void);//准备抓一层的状态




void In_1(void);//将手中的物块放入物料盘1，爪子张开回到0度 高度为货架二层高度 物料盘转为2
void In_2(void);//
void In_3(void);//

void Down_In_1(void);//将手中的物块放入物料盘1，爪子张开回到0度 高度为货架一层高度 物料盘转为2
void Down_In_2(void);//
void Down_In_3(void);//

void Out_1(void);//将物料盘1中的物块取到0度，准备摆放物块 夹着物块
void Out_2(void);//
void Out_3(void);//

void Out_1_back(void);//fang放回去
void Out_2_back(void);//
void Out_3_back(void);//


void Out_1_MD(void);//将物料盘1中的物块取到0度，准备摆放物块 夹着物块 码垛
void Out_2_MD(void);//
void Out_3_MD(void);//

void wait_state(uint16_t state);//死循环等待动作完成




//=============================标志位=======================
extern uint8_t Catch_uP;
extern uint8_t Fang_state;
extern uint8_t IN_upcolor1 ;
extern uint8_t IN_upcolor2 ;
extern uint8_t IN_upcolor3 ;
extern uint8_t OUT_1;
extern uint8_t OUT_2;
extern uint8_t OUT_3;
extern uint8_t OUT_1MD;
extern uint8_t OUT_2MD;
extern uint8_t OUT_3MD;
extern uint8_t Out1_back;
extern uint8_t Out2_back;
extern uint8_t Out3_back;
extern uint16_t delay_tim_90_0du ;//
extern uint8_t IN_downcolor1 ;
extern uint8_t IN_downcolor2 ;
extern uint8_t IN_downcolor3 ;
/*
舵机调节：
物料盘（S4）：77， 167， 260
存储值的变量：uint8_t color_save[3]

初始化：
爪子开合 s1：91
升降s2：60
俯仰臂s3：220
载物台：77， 167， 260*/
//Servo_Move(TIM_CHANNEL_1,180,3);//爪子160 张的最开    95抓紧  105 抓着松一点
//Servo_Move(TIM_CHANNEL_2,ss2,2);//升降 100低   250高   物理最低限位：55           (<300 >50)
//Servo_Move(TIM_CHANNEL_3,ss3,2);//折叠  180:90度   70:0度    
//Servo_Move(TIM_CHANNEL_4,ss4,2);//折叠 50 红    160 绿  250 蓝
/*
放物块地板：ss1:95  55   65   
码垛高度：      ss2：160
货架二层高度 ss2：295  ss3:70-》65    ss1：160
货架一层   ：160  90 70-》65  

抓取之后放到物料盘：
95  295   290  A77   抓住 抬升  180度 
180 295  70   A70   放开  0度

*/
#endif




