#include "line_track.h"
#include "catch.h"
#include "tim.h"
uint16_t ss1=160;
uint16_t ss2=90;
uint16_t ss3=65;
uint16_t ss4=70;
uint16_t sped=5;
uint8_t Catch_uP = 0;
uint8_t Fang_state = 0;

uint8_t IN_upcolor1 =5;
uint8_t IN_upcolor2 =6;
uint8_t IN_upcolor3 =7;

uint8_t IN_downcolor1 =8;
uint8_t IN_downcolor2 =9;
uint8_t IN_downcolor3 =10;

uint8_t OUT_1=11;
uint8_t OUT_2=12;
uint8_t OUT_3=13;
uint8_t Out1_back=17;
uint8_t Out2_back=18;
uint8_t Out3_back=19;

uint8_t OUT_1MD=14;
uint8_t OUT_2MD=15;
uint8_t OUT_3MD=16;


uint16_t delay_tim = 800;//升降需要的时间
uint16_t delay_tim1 = 400;//旋转180度需要的时间   没抓物块的时间

uint16_t delay_tim2 = 250;//抓物块需要的时间  
uint16_t delay_tim3 = 500;//旋转0度的时间  抓着物块
uint16_t delay_top_md = 600;//top到码垛高度的时间 

uint16_t delay_tim4 = 400;//旋转90du 
uint16_t delay_tim6 = 1000;//
uint16_t delay_tim_90_0du = 100;//

uint16_t delay_tim7 = 400;//旋转180度需要的时间   没抓物块的时间
uint16_t delay_tim8 = 400;//旋转180度需要的时间   没抓物块的时间
uint16_t delay_tim9 = 400;//旋转180度需要的时间   没抓物块的时间


uint16_t OPEN_DELAY = 200;//旋转180度需要的时间   没抓物块的时间

uint16_t dp_DELAY = 200;//旋转180度需要的时间   没抓物块的时间
 extern uint8_t close_delay ;
 


void wait_state(uint16_t state)//死循环等待动作完成
{
	while(state);
	
}
void Close(int speed)
{
Servo_Move(TIM_CHANNEL_1,Close_1,3);
//	TIM4->CCR1=Close_1;
	//else if(a==1)Servo_Move(TIM_CHANNEL_1,Close_2,3);//
// vTaskDelay(close_delay);
	vTaskDelay(400);
}
void Close_alit(int speed)//吊着物块
{
Servo_Move(TIM_CHANNEL_1,Close_2,speed);//

}
void Open(void)//准备抓东西
{
if(two_3_1==1&&car_palce==2)Servo_Move(TIM_CHANNEL_1,Open_2,0);//放东西 Open_2
else if(car_palce==12||car_palce==23||car_palce==3)Servo_Move(TIM_CHANNEL_1,Open_2,0);//放东西 Open_2
	
else Servo_Move(TIM_CHANNEL_1,Open_1,0);//抓东西 Open_1
	
vTaskDelay(OPEN_DELAY);

}
void Fang_slow3(void)//f慢慢放东西
{
if(updowm==0)//地板
{
_e15D(0);//wangxia往下铁地板  不扔
	vTaskDelay(200);
}

	
	TIM4->CCR1=Open_2;
	vTaskDelay(200);
//Turning_Laps(10,min_speed,Back);	
//vTaskDelay(delay_tim2);
_90D(0);
vTaskDelay(delay_tim4);
	
	
}

//区域2放东西
void Fang_slow2(void)//f慢慢放东西
{

_e15D(1);//wangxia往下铁地板  不扔
	vTaskDelay(200);
	TIM4->CCR1=Open_2;
	vTaskDelay(200);
Turning_Laps(15,min_speed,Back);	
_90D(0);
vTaskDelay(delay_tim4);
	
}


void Fang_slow(void)//f慢慢放东西
{

_e15D(1);//wangxia往下铁地板  不扔
	vTaskDelay(200);
	TIM4->CCR1=Open_2;
	vTaskDelay(200);
Turning_Laps(15,min_speed,Back);	
//vTaskDelay(delay_tim2);
_90D(0);
vTaskDelay(delay_tim4);
	
}
void Fang_slow0(void)//放完不90度
{

_e15D(1);//wangxia往下铁地板  不扔
	vTaskDelay(200);
	TIM4->CCR1=Open_1;
	vTaskDelay(200);
Turning_Laps(15,min_speed,Back);	


	
}

void High_groud(int speed)
{
Servo_Move(TIM_CHANNEL_2,Ground,speed);
}
void High_groud2(int speed)
{
Servo_Move(TIM_CHANNEL_2,Ground2,speed);

}
void High_up1(int speed)
{Servo_Move(TIM_CHANNEL_2,Huojia_1,speed);
}
void High_up2(int speed)
{Servo_Move(TIM_CHANNEL_2,Huojia_2,speed);

}
void TOP1(int speed)
{
Servo_Move(TIM_CHANNEL_2,TOP,speed);
}

void _0D(int speed)
{
	Servo_Move(TIM_CHANNEL_3,s3_0d,speed);//折叠  180:90度   70:0度  
}
void _90D(int speed)
{
	Servo_Move(TIM_CHANNEL_3,s3_90d,speed);//折叠  180:90度   70:0度  
}
void _e15D(int speed){
	
Servo_Move(TIM_CHANNEL_3,s3_f5d,speed);//折叠  180:90度   70:0度  
}
void _180D(int speed){
	Servo_Move(TIM_CHANNEL_3,s3_180d,speed);//折叠  180:90度   70:0度  
}

void _DP1(int speed)
{
	Servo_Move(TIM_CHANNEL_4,Duopan_1,speed);//折叠 50 红    160 绿  250 蓝
}
void _DP2(int speed)
	{
		Servo_Move(TIM_CHANNEL_4,Duopan_2,speed);//折叠 50 红    160 绿  250 蓝
	}
void _DP3(int speed)
	{
		Servo_Move(TIM_CHANNEL_4,Duopan_3,speed);//折叠 50 红    160 绿  250 蓝
	}
void Catch_up2_really(void)//准备抓二层的状态
{
	_DP1(0);
	Close(1);
	High_up2(3);

	_0D(3);

		Open();
//Servo_Move(TIM_CHANNEL_2,Huojia_2,3);//升降 100低   250高   物理最低限位：55                (<300 >50)
//Servo_Move(TIM_CHANNEL_1,Open_1,3);//爪子160 张的最开    95抓紧  105 抓着松一点
//Servo_Move(TIM_CHANNEL_3,s3_0d,3);//折叠  180:90度   70:0度    
//Servo_Move(TIM_CHANNEL_4,Duopan_1,2);//折叠 50 红    160 绿  250 蓝

}

void Catch_up1_really(void)//准备抓一层的状态
{
		High_up1(3);
	Open();
	_0D(3);
	
	High_up1(3);
	_DP1(3);

	
	
}


void In_1(void)//将手中的物块放入物料盘，爪子回到0度
{
	TOP1(2);
_180D(3);
	vTaskDelay(400);
	Open();
	
	_0D(3);	
  _DP2(1);
}
void In_2(void)//
{
		TOP1(2);
	_180D(3);
		vTaskDelay(400);
	Open();
	_0D(3);	
  _DP3(1);


}
void In_3(void)//
{
		TOP1(2);
	_180D(3);
		vTaskDelay(400);
	Open();
	_90D(3);	
  _DP1(1);
		vTaskDelay(dp_DELAY);
}
void Down_In_1(void)//将手中的物块放入物料盘1，爪子回到0度 高度为货架一层高度
{
	_DP1(1);
	Close(1);


_180D(3);
			vTaskDelay(400);
	Open();
	_0D(2);	
//	High_up1(2);
  _DP2(1);
	
}
void Down_In_2(void){

	Close(1);

_180D(3);
			vTaskDelay(400);
	Open();
	_0D(2);	

  _DP3(1);
}
void Down_In_3(void){

		Close(1);
		TOP1(2);
		vTaskDelay(delay_tim);
_180D(3);
			vTaskDelay(400);
	Open();
	_90D(3);	
  _DP1(1);
	vTaskDelay(dp_DELAY);

}

void Out_1(void)//将物料盘1中的物块取到0度，准备摆放物块
{	_DP1(0);
	
	TOP1(3);
	Open();
	vTaskDelay(200);
	
	_180D(1);
			vTaskDelay(delay_tim3);
	Close(1);

	_0D(1);
	
	vTaskDelay(delay_tim3);
	High_groud(1);
	_DP2(0);

	vTaskDelay(delay_tim6);
}
void Out_2(void)//
{
	_DP2(1);
	TOP1(3);

		vTaskDelay(delay_tim);
	_180D(1);
	vTaskDelay(delay_tim1);
	Close(1);
		vTaskDelay(delay_tim2);
	
	_0D(1);
	vTaskDelay(delay_tim3);
	High_groud(1);
	_DP3(1);

	vTaskDelay(delay_tim);
	
}
void Out_3(void)//
{
	_DP3(1);
		TOP1(3);
			vTaskDelay(delay_tim);
	_180D(1);
			vTaskDelay(delay_tim1);
	Close(1);
		vTaskDelay(delay_tim2);

		_0D(1);
	vTaskDelay(delay_tim3);
	High_groud(1);

	_DP1(1);
	
	vTaskDelay(delay_tim);
}
void Out_1_back(void)//fang放回去
{

	
	_90D(0);
	TOP1(0);
	vTaskDelay(delay_tim);
	_180D(0);
	vTaskDelay(delay_tim3);
	Open();
	vTaskDelay(delay_tim2);
	_e15D(0);
	vTaskDelay(delay_tim1);
High_groud(0);
	_DP2(0);
		vTaskDelay(delay_tim);
}
void Out_2_back(void){

			_DP2(1);
		_90D(1);
TOP1(1);
		vTaskDelay(delay_tim);
	_180D(1);
		vTaskDelay(delay_tim3);
	Open();
		vTaskDelay(delay_tim2);
		_e15D(2);
		vTaskDelay(delay_tim1);
	High_groud(1);
	_DP3(1);
			vTaskDelay(delay_tim);
}
void Out_3_back(void){

		_DP3(1);
		_90D(1);
TOP1(1);
			vTaskDelay(delay_tim);
	_180D(1);
			vTaskDelay(delay_tim3);

	Open();
		vTaskDelay(delay_tim2);
	_90D(2);


	_DP1(1);
	vTaskDelay(dp_DELAY);
}
void Out_1_MD(void)//将物料盘1中的物块取到0度，准备摆放物块 夹着物块 码垛
{
	
	TOP1(3);
	Open();
	vTaskDelay(delay_tim);
	_180D(1);
			vTaskDelay(delay_tim3);
		Close(1);
		vTaskDelay(delay_tim2);
	_0D(1);
	vTaskDelay(delay_tim3);
	High_groud2(3);
			_DP2(0);
	vTaskDelay(delay_top_md);
}
void Out_2_MD(void)//
{

	TOP1(1);
			vTaskDelay(delay_tim);
		_180D(1);
	vTaskDelay(delay_tim1);
		Close(1);
		vTaskDelay(delay_tim2);
	_0D(1);
	vTaskDelay(delay_tim3);
	High_groud2(3);
			_DP3(0);
	vTaskDelay(delay_top_md);
	
}
void Out_3_MD(void)//
{
	_DP3(0);

	TOP1(1);
			vTaskDelay(delay_tim);
		_180D(1);
	vTaskDelay(delay_tim1);
		Close(2);
		vTaskDelay(delay_tim2);
	_0D(2);
	vTaskDelay(delay_tim3);
	High_groud2(3);
		
	vTaskDelay(delay_top_md);
}



