/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "bsp_can.h"
#include "pid.h"
#include "bsp_key.h"
#include "Gui.h"
#include "lcd.h"
#include "tim.h"
#include "stdio.h"
#include "math.h"
#include "manu.h"
#include "line_track.h"
#include "move_control.h"
#include "maintask.h"
uint8_t test1=50;
uint8_t test2;


uint8_t Color;
uint8_t Color_c;
extern pid_struct_t motor_pid[7];
extern moto_info_t motor_info[MOTOR_MAX_NUM];
uint16_t stop;
int ch[4];
int ch4[4];
float ch1[2];
uint8_t begin_flag = 0;
extern All_car_Typedef car;
volatile uint8_t color_save[3];
volatile uint8_t position[3];

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osThreadId myTaskINHandle;
osThreadId myTaskOUTHandle;
osThreadId myTaskCtrlHandle;
osThreadId myTask05Handle;
osThreadId myTask06Handle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void StartTaskIN(void const * argument);
void StartTaskOUT(void const * argument);
void StartTaskCtrl(void const * argument);
void StartTask05(void const * argument);
void StartTask06(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of myTaskIN */
  osThreadDef(myTaskIN, StartTaskIN, osPriorityIdle, 0, 128);
  myTaskINHandle = osThreadCreate(osThread(myTaskIN), NULL);

  /* definition and creation of myTaskOUT */
  osThreadDef(myTaskOUT, StartTaskOUT, osPriorityIdle, 0, 128);
  myTaskOUTHandle = osThreadCreate(osThread(myTaskOUT), NULL);

  /* definition and creation of myTaskCtrl */
  osThreadDef(myTaskCtrl, StartTaskCtrl, osPriorityIdle, 0, 128);
  myTaskCtrlHandle = osThreadCreate(osThread(myTaskCtrl), NULL);

  /* definition and creation of myTask05 */
  osThreadDef(myTask05, StartTask05, osPriorityIdle, 0, 128);
  myTask05Handle = osThreadCreate(osThread(myTask05), NULL);

  /* definition and creation of myTask06 */
  osThreadDef(myTask06, StartTask06, osPriorityIdle, 0, 128);
  myTask06Handle = osThreadCreate(osThread(myTask06), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
   
    osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartTaskIN */
/**
* @brief Function implementing the myTaskIN thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTaskIN */
void StartTaskIN(void const * argument)
{
  /* USER CODE BEGIN StartTaskIN */
  /* Infinite loop */
  for(;;)
  {


		Manu_Interface();  //oled菜单1
    osDelay(1);
  }
  /* USER CODE END StartTaskIN */
}

/* USER CODE BEGIN Header_StartTaskOUT */
/*
  3  *  0  
***********
  2  *  1  
*/
/**
* @brief Function implementing the myTaskOUT thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTaskOUT */
void StartTaskOUT(void const * argument)
{
  /* USER CODE BEGIN StartTaskOUT */
  /* Infinite loop */
  for(;;)//2电机驱动底层
  {  
		switch(car.drive){
			case Forward:
				ch[0]=-car.speed_R;
				ch[1]=-car.speed_R;
				ch[2]=car.speed_L;
				ch[3]=car.speed_L;
				break;
				case Forward1:
				ch[0]=-car.speed_RF;
				ch[1]=-car.speed_RB;
				ch[2]=car.speed_LB;
				ch[3]=car.speed_LF;
				break;
				case Left1:
				ch[0]=-car.speed_RB;
				ch[1]=car.speed_LB;
				ch[2]=car.speed_LF;
				ch[3]=-car.speed_RF;			
				break;
			case Back:
				ch[0]=car.speed_L;
				ch[1]=car.speed_L;
				ch[2]=-car.speed_R;
				ch[3]=-car.speed_R;
				break;
			case Left:
				ch[0]=-car.speed_R;
				ch[1]=car.speed_L;
				ch[2]=car.speed_L;
				ch[3]=-car.speed_R;			
				break;

			case Right:
				ch[0]=car.speed_L;
				ch[1]=-car.speed_R;
				ch[2]=-car.speed_R;
				ch[3]=car.speed_L;			
				break;
			case Turn_L:
				ch[0]=-car.speed_T;
				ch[1]=-car.speed_T;
				ch[2]=-car.speed_T;
				ch[3]=-car.speed_T;				
				break;
			case Turn_L1:
				ch[0]=-car.speed_R;
				ch[1]=-car.speed_R;
				ch[2]=-car.speed_L;
				ch[3]=-car.speed_L;				
				break;
			case Turn_L2:
				ch[0]=-car.speed_RF;
				ch[1]=-car.speed_RB;
				ch[2]=-car.speed_LB;
				ch[3]=-car.speed_LF;		
				break;						
			case Turn_R:
				ch[0]=car.speed_T;
				ch[1]=car.speed_T;
				ch[2]=car.speed_T;
				ch[3]=car.speed_T;				
				break;
			case Sloping_L_F:
				ch[0]=-car.speed_T;
				ch[1]=0;
				ch[2]=car.speed_T;
				ch[3]=0;
				break;
			case Sloping_R_F:
				ch[0]=0;
				ch[1]=-car.speed_T;
				ch[2]=0;
				ch[3]=car.speed_T;
				break;
			case Sloping_L_B:
				ch[0]=0;
				ch[1]=car.speed_T;
				ch[2]=0;
				ch[3]=-car.speed_T;
				break;
			case Sloping_R_B:
				ch[0]=car.speed_T;
				ch[1]=0;
				ch[2]=-car.speed_T;
				ch[3]=0;
			default:
				break;
		}
		motor_info[0].set_voltage = pid_calc(&motor_pid[0], ch[0], motor_info[0].rotor_speed);
		motor_info[1].set_voltage = pid_calc(&motor_pid[1], ch[1], motor_info[1].rotor_speed);
		motor_info[2].set_voltage = pid_calc(&motor_pid[2], ch[2], motor_info[2].rotor_speed);
		motor_info[3].set_voltage = pid_calc(&motor_pid[3], ch[3], motor_info[3].rotor_speed);
  
    /* send motor control message through can bus*/
    set_motor_voltage(0, 
                      motor_info[0].set_voltage, 
                      motor_info[1].set_voltage, 
                      motor_info[2].set_voltage, 
                      motor_info[3].set_voltage);
   vTaskDelay(2);
  }
  /* USER CODE END StartTaskOUT */
}

/* USER CODE BEGIN Header_StartTaskCtrl */
/*

  3  *  0  
***********
  2  *  1  

uint16_t  speed_L;   左边速度
uint16_t  speed_R;   右边速度
uint16_t  speed_T;   旋转或斜走速度
uint8_t   Distance_B;
uint16_t  Distance;
uint8_t   over;
car.drive=Forward;车的反向
    Forward = 0x00,  前进
    Back = 0x01,     后退
    Left = 0x02,     左
    Right = 0x03,    右
    Turn_L = 0x04,   左转
    Turn_R = 0x05,   右转
    Sloping_L_F = 0x06, 左前斜走
    Sloping_R_F = 0x07, 右前斜走
    Sloping_L_B = 0x08, 左后斜走
    Sloping_R_B = 0x09, 右后斜走

void Turning_Laps(uint16_t num,float speed,car_Typedef fx)  num 圈数  speed 速度  fx 方向

*Serbor(Paws_OFF,Lifting_UP,Flip_UP,Turntable_0);

*float Pre_range;   前测距
*float Bak_range;   后测距
*float Left_range;  左测距
*float Right_range; 右测距

*ADC_Value[1~8] 灰度ADC值
***************
*  14 0 1 2   *
*12 DU1  DU2 4*
*11          3*
*13          8*
* 10  9 6 7   *
***************
ADC_Value[9] 摇滚X的偏移量
ADC_Value[10] 摇滚Y的偏移量
key_scan   =1 按键按下
           =0 无按下
DU1 预留数线1
DU2 预留数线1
DU3 预留数线1

Color =1 红
Color =2 绿
Color =3 蓝

UART2_RECV_BUF[20] //例如 213+321；//二维码接受到的数据。

LCD显示在Gui.c

舵机调节：
物料盘（S4）：77， 167， 260
存储值的变量：uint8_t color_save[3]

初始化：
爪子开合 s1：91
升降s2：60
俯仰臂s3：220
载物台：77， 167， 260

*/
/**
* @brief Function implementing the myTaskCtrl thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTaskCtrl */
void StartTaskCtrl(void const * argument)
{
  /* USER CODE BEGIN StartTaskCtrl */
  /* Infinite loop */

  for(;;)
  {	
		Task_one();


while(1)
   osDelay(1);
}
  /* USER CODE END StartTaskCtrl */
}

/* USER CODE BEGIN Header_StartTask05 */
/**
* @brief Function implementing the myTask05 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask05 */
void StartTask05(void const * argument)
{
  /* USER CODE BEGIN StartTask05 */
  /* Infinite loop */
  for(;;)
  {
		Catch_task();
    osDelay(1);
  }
  /* USER CODE END StartTask05 */
}

/* USER CODE BEGIN Header_StartTask06 */
/**
* @brief Function implementing the myTask06 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask06 */
void StartTask06(void const * argument)
{
  /* USER CODE BEGIN StartTask06 */
  /* Infinite loop */
  for(;;)
  { 
//		Task_two();
    osDelay(1);
  }
  /* USER CODE END StartTask06 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
