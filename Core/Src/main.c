/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "adc.h"
#include "can.h"
#include "dma.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "bsp_can.h"
#include "pid.h"
#include "bsp_key.h"
#include "math.h"
#include "Gui.h"
#include "line_track.h"
#include "lcd.h"
#include "oled.h"
#include "stdio.h"
#include "stdlib.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
uint8_t rebuf[20];
RGB rgb;
RGB rgb_c;
unsigned char UART_BUF[8]={0x01,0x03,0x00,0x0F,0x00,0x02,0xF4,0x08};
unsigned char UART6_RECV_BUF[20];//串口6 接受 发送缓存
unsigned char UART2_RECV_BUF[20];//串口6 接受 发送缓存    任务抓取顺序
unsigned char UART7_RECV_BUF[128];//串口7 接受 发送缓存
pid_struct_t motor_pid[7];
extern moto_info_t motor_info[MOTOR_MAX_NUM];
extern moto_info_t motor_info1[MOTOR_MAX_NUM];
uint16_t pwm_pulse = 200;
extern uint16_t stop;
int32_t set_spd = 0;
int x;
uint16_t TIM_COUNT[2];
uint16_t ADC_Value[15];
uint16_t ADC_Value1[2];
int32_t temp1;
float Pre_range;
float Bak_range;
float Left_range;
float Right_range;
float Pre_range1;
float Bak_range1;
float Left_range1;
float Right_range1;
All_car_Typedef car;		
uint32_t err = 0;
uint32_t s = 0;
uint16_t shang = 0;
uint16_t s1 = 0;
int c;
float xL;
extern float ch1[2];
extern int ch4[4];
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
int fputc(int ch,FILE *f)
{
while (__HAL_UART_GET_FLAG(&huart6,UART_FLAG_TXE)==RESET||__HAL_UART_GET_FLAG(&huart7,UART_FLAG_TXE)==RESET){}
       huart6.Instance->DR=ch;
       huart7.Instance->DR=ch;
	return ch;
}
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
//	SCB->VTOR = FLASH_BASE | 0x10000;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_CAN1_Init();
  MX_TIM4_Init();
  MX_UART7_Init();
  MX_UART8_Init();
  MX_USART6_UART_Init();
  MX_CAN2_Init();
  MX_SPI4_Init();
  MX_SPI1_Init();
  MX_USART2_UART_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
//	can_user_init(&hcan1);       //can传输初始化
//	can_user_init(&hcan2);       //can传输初始化
  HAL_UART_Receive_IT_IDLE(&huart1,UART_Buffer,100);   //启动串口接收     ③          遥控器
  HAL_GPIO_WritePin(GPIOH, POWER_1_Pin|POWER_2_Pin|POWER_3_Pin|POWER_4_Pin, GPIO_PIN_SET);  //电机四路电压初始化
  can_filter_init();
	pwm_Init();      //pwm初始化
	HAL_UART_DMAPause(&huart8);   //暂停DMA传输，可以在不进行颜色识别的时候关闭。
	LCD_Init();
  oled_init();                 //oled初始化
  oled_clear(Pen_Clear);       //oled清屏
	for (uint8_t i = 0; i < 7; i++){                 //电机pid参数初始
    pid_init(PID_Speed,&motor_pid[i], 5,0.0001f,10, 8000, 8000,0); 
  } 
	shang=motor_info[0].rotor_angle;
  HAL_UART_Receive_DMA(&huart8,rebuf,20);          //串口八DMA接收初始化
  __HAL_UART_ENABLE_IT(&huart6,UART_IT_IDLE);      //串口六使能
  __HAL_UART_ENABLE_IT(&huart2,UART_IT_IDLE);      //串口六使能
  __HAL_UART_ENABLE_IT(&huart7,UART_IT_IDLE);      //串口七使能
  __HAL_UART_ENABLE_IT(&huart8,UART_IT_IDLE);      //串口八使能
	HAL_UART_Receive_DMA(&huart2,UART2_RECV_BUF,20);//串口六DMA接收初始化
	HAL_UART_Receive_DMA(&huart6,UART6_RECV_BUF,20);//串口六DMA接收初始化
	HAL_UART_Receive_DMA(&huart7,UART7_RECV_BUF,20);//串口七DMA接收初始化
	HAL_UART_DMAResume(&huart8);  //恢复DMA传输，可以在进行颜色识别的时候开启
	HAL_ADC_Start(&hadc1);
 	HAL_ADC_Start_DMA(&hadc1,(uint32_t*)&ADC_Value1,2);
//	HAL_Delay(2000);
//  printf("AT+RST\r\n");
//	HAL_Delay(5000);
//   printf("AT+CWMODE=1\r\n");
//	 HAL_Delay(5000);
//   printf("AT+CWJAP=\"FAST_48A7A0\",\"12345678\"\r\n");
//	 HAL_Delay(5000);
//	 HAL_Delay(5000);
//   printf("AT+CIPSTART=\"UDP\",\"192.168.1.255\",10000,4210,0\r\n");
//	 HAL_Delay(5000);
//   printf("AT+CIPMODE=1\r\n");
//	printf("iACM");
	//HAL_UART_Transmit(&huart7,UART_BUF,8,1000);
  /* USER CODE END 2 */

  /* Call init function for freertos objects (in freertos.c) */
  MX_FREERTOS_Init();
  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 6;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void Turning_Laps(uint16_t num,float speed,car_Typedef fx)//50/格   1：num 2006圈数   fx车状态：车走向 
{     
		int32_t s = 0;
	int32_t stang = 0;


	if(fx==Forward||fx==Back||fx==Left||fx==Right)
	{
	car.speed_L=speed;
	car.speed_R=speed;
	car.drive=fx;
		
		stang=motor_info1[0].round_cnt;//jilu记录走过的编码器个数    -5
	while(1){
		err=motor_info1[0].round_cnt-stang;//和原来记录的编码器个数的差   ==走i过的圈数
//		err=motor_info1[0].rotor_angle-stang;
//		if(err>7500){
//				s=s+1;
//		}
		if(err>=num){//当差值大于用户的num时，说明已经走过了目标编码器圈数，停车
				car.speed_L=0;		
				car.speed_R=0;
				break;
		}
		vTaskDelay(2);
	}
}
	else 
	{
	car.speed_T=speed;
	car.drive=fx;
	while(1){
		
		if(fx==Sloping_L_F||fx==Sloping_R_B)//斜着走的补充
		err=abs(motor_info1[0].rotor_angle-stang);
		else
		err=abs(motor_info1[1].rotor_angle-stang);
		
		if(err>7500){
				s=s+1;
		}
		if(s>=num){
  Stop();
				break;
		}
		vTaskDelay(2);
	}		
}
	
///////////////////////////////
//	int32_t s = 0;
//	int32_t stang = 0;


//	if(fx==Forward||fx==Back||fx==Left||fx==Right)
//	{
//	car.speed_L=speed;
//	car.speed_R=speed;
//	car.drive=fx;
//	while(1){
//		err=motor_info1[0].rotor_angle-stang;
//		if(err>7500){
//				s=s+1;
//		}
//		if(s>=num){
//				car.speed_L=0;		
//				car.speed_R=0;
//				break;
//		}
//		vTaskDelay(2);
//	}
//}
//	else 
//	{
//	car.speed_T=speed;
//	car.drive=fx;
//	while(1){
//		
//		if(fx==Sloping_L_F||fx==Sloping_R_B)//斜着走的补充
//		err=abs(motor_info1[0].rotor_angle-stang);
//		else
//		err=abs(motor_info1[1].rotor_angle-stang);
//		
//		if(err>7500){
//				s=s+1;
//		}
//		if(s>=num){
//  Stop();
//				break;
//		}
//		vTaskDelay(2);
//	}		
//}
//	



}

void Turning_Laps_T(uint16_t num,float speed,car_Typedef fx)
{     
	int32_t s = 0;
	int32_t stang = 0;
	car.drive=fx;
	car.speed_T=speed;
//	car.speed_T=speed;
	
	while(1){
		if(fx==Sloping_L_F||fx==Sloping_R_B)
		err=abs(motor_info1[0].rotor_angle-stang);
		else
		err=abs(motor_info1[1].rotor_angle-stang);
		
		if(err>7500){
				s=s+1;
		}
		if(s>=num){
  Stop();
				break;
		}
		vTaskDelay(2);
	}
}

int KEY; 
int Get_Key(void)
{
    if(ADC_Value1[0]<=420)  KEY=5;//确定
		else if(ADC_Value1[0]<=1000)  KEY=3;  //左
		else if(ADC_Value1[0]<=1900)  KEY=4; //右
		else if(ADC_Value1[0]<=2600)  KEY=1; //上
		else if(ADC_Value1[0]<=3050)  KEY=2; //下
		else KEY=0;
    return KEY;
}
/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
