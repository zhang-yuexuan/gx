/**
  ******************************************************************************
  * File Name          : TIM.c
  * Description        : This file provides code for the configuration
  *                      of the TIM instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "tim.h"

/* USER CODE BEGIN 0 */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "stack_macros.h"
/* USER CODE END 0 */

TIM_HandleTypeDef htim4;

/* TIM4 init function */
void MX_TIM4_Init(void)
{
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 720-1;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 2000-1;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  HAL_TIM_MspPostInit(&htim4);

}

void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef* tim_pwmHandle)
{

  if(tim_pwmHandle->Instance==TIM4)
  {
  /* USER CODE BEGIN TIM4_MspInit 0 */

  /* USER CODE END TIM4_MspInit 0 */
    /* TIM4 clock enable */
    __HAL_RCC_TIM4_CLK_ENABLE();
  /* USER CODE BEGIN TIM4_MspInit 1 */

  /* USER CODE END TIM4_MspInit 1 */
  }
}
void HAL_TIM_MspPostInit(TIM_HandleTypeDef* timHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(timHandle->Instance==TIM4)
  {
  /* USER CODE BEGIN TIM4_MspPostInit 0 */

  /* USER CODE END TIM4_MspPostInit 0 */

    __HAL_RCC_GPIOD_CLK_ENABLE();
    /**TIM4 GPIO Configuration
    PD15     ------> TIM4_CH4
    PD14     ------> TIM4_CH3
    PD13     ------> TIM4_CH2
    PD12     ------> TIM4_CH1
    */
    GPIO_InitStruct.Pin = GPIO_PIN_15|GPIO_PIN_14|GPIO_PIN_13|GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM4;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /* USER CODE BEGIN TIM4_MspPostInit 1 */

  /* USER CODE END TIM4_MspPostInit 1 */
  }

}

void HAL_TIM_PWM_MspDeInit(TIM_HandleTypeDef* tim_pwmHandle)
{

  if(tim_pwmHandle->Instance==TIM4)
  {
  /* USER CODE BEGIN TIM4_MspDeInit 0 */

  /* USER CODE END TIM4_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM4_CLK_DISABLE();
  /* USER CODE BEGIN TIM4_MspDeInit 1 */

  /* USER CODE END TIM4_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */
void pwm_Init(void)
{
    HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1); // צ��  H
    HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2); //SHEN���� G
    HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_3);  //XUAN��ת   F
    HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_4);  //����   E
}

void Servo_Move(int channel,int angle,int k)
{
	switch(channel)
	{
		case TIM_CHANNEL_1:	
			if(angle>TIM4->CCR1)
			{
				for(;TIM4->CCR1<angle;TIM4->CCR1++){
    vTaskDelay(k);
			}
				
			}
			else if(angle<TIM4->CCR1)
			{
				for(;TIM4->CCR1>angle;TIM4->CCR1--){
    vTaskDelay(k);
			}
			}
			break;
		case TIM_CHANNEL_2:
			if(angle>TIM4->CCR2)
			{
				for(;TIM4->CCR2<angle;TIM4->CCR2++){
    vTaskDelay(k);
			}
			}
			else if(angle<TIM4->CCR2)
			{
				for(;TIM4->CCR2>angle;TIM4->CCR2--){
    vTaskDelay(k);
			}
			}
			break;
		case TIM_CHANNEL_3:
			if(angle>TIM4->CCR3)
			{
				for(;TIM4->CCR3<angle;TIM4->CCR3++){
    vTaskDelay(k);
			}
			}
			else if(angle<TIM4->CCR3)
			{
				for(;TIM4->CCR3>angle;TIM4->CCR3--){
    vTaskDelay(k);
			}
			}
			break;
		case TIM_CHANNEL_4:
			if(angle>TIM4->CCR4)
			{
				for(;TIM4->CCR4<angle;TIM4->CCR4++){
    vTaskDelay(k);
			}
			}
			else if(angle<TIM4->CCR4)
			{
				for(;TIM4->CCR4>angle;TIM4->CCR4--){
    vTaskDelay(k);
			}
			}
			break;
	}
}
void Serbor(int s1,int s2,int s3,int s4,int speed)
{
	Servo_Move(TIM_CHANNEL_1,s1,speed);
	Servo_Move(TIM_CHANNEL_2,s2,speed);
	Servo_Move(TIM_CHANNEL_3,s3,speed);
	Servo_Move(TIM_CHANNEL_4,s4,speed);
}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
