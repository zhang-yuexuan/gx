# GX（2022上海工程训练综合能力竞赛“智能+”赛道）

## 基本介绍

* 本项目为上海最终决赛代码
* 开发板：大疆A板
* 电机：大疆2006
* 二维码模块：串口二维码识别模块
* 舵机：普通sg90舵机
* 其他资源：车载激光测距，激光灰度，模拟灰度

## 软件框架与程序模块

### 多线程

#### 线程一：底层电机驱动

```cpp
    set_motor_voltage(0, 
                      motor_info[0].set_voltage, 
                      motor_info[1].set_voltage, 
                      motor_info[2].set_voltage, 
                      motor_info[3].set_voltage);
```

功能：底层电机驱动线程

#### 线程二：主任务线程

```cpp
Task_one();
```

功能：整个比赛任务流程

#### 线程三：辅助任务线程

```cpp
	Catch_task();
```

功能：辅助主任务的线程，主线程发送命令过来，辅助线程操作对应的任务，实现双线程完成任务，大大缩短时间

其他：进程间通信采用宏定义变量实现参数传递

#### 线程四：oled菜单线程

```cpp
Manu_Interface();  //oled菜单
```

功能：作为ui控制舵机和所有灰度

---

### 编码器运动模块

```cpp
Turning_Laps(100,3000, Left );//编码器旋转圈数 车辆运行速度 方向
```

```cpp

void Turning_Laps(uint16_t num,float speed,car_Typedef fx)//50/格   1：num 2006圈数   fx车状态：车走向 
{   
	int32_t s = 0;
	int32_t stang = 0;//标记起点
	if(fx==Forward||fx==Back||fx==Left||fx==Right)
	{
	car.speed_L=speed;
	car.speed_R=speed;
	car.drive=fx;//车辆行驶的方向和速度
		stang=motor_info1[0].round_cnt;//标记起点cnt
	        while(1){
		err=motor_info1[0].round_cnt-stang;//err=终点cnt-起点cnt
		if(err>=num){//当err>=num时，终点-起点=num，停车
				car.speed_L=0;
				car.speed_R=0;
				break;
		}
		vTaskDelay(2);
	}
}}
```

优化：在can接收回调函数内进行编码器圈数的判断（实时性高，不容易数错）

```cpp
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
。。。
		if(motor_info1[index].rotor_angle -motor_info1[index].last_angle>4096)//判断已经转过一圈
		{motor_info1[index].round_cnt ++;motor_info[index].round_cnt --;}//round_cnt编码器圈数+1
		else if(motor_info1[index].rotor_angle -motor_info1[index].last_angle<-4096)
		{motor_info1[index].round_cnt --;motor_info[index].round_cnt --;}
}
```

---

### 数线循迹模块

```cpp
void TraceL_FAST_Num(uint16_t num,float speed)//寻迹数线 num：过几条线  speed：速度
{
	int8_t  u=0;//u的作用：防止重复数线（同一根线只数一次），功能：由白到黑的过程才会数一根
		while(num)
		{
                TraceL_speed(speed);循迹函数
		if(DUI0 ==1)//LEFT3
		{
		HAL_Delay(10);//消抖
			if(DUI0 ==1)
			{
				if(u==1)//  last  
				{
					num--;
					u=0;
				}
			}
		}
		else u=1;
		}
	Stop();

}
```

循迹函数矫正方案：

车头向左矫正：左前轮减速，右前轮加速，后轮两个不变

```cpp
void TraceL_speed(uint16_t speed)
{/*
		DUB0
  	DUC2
	  DUB1
		DUC3	  
	*/
			Huidu_L=(DUC3<<3)|(DUB1<<2)|(DUC2<<1)|DUB0; 
	  car.drive=Left1;

				switch(Huidu_L)
	{
		case 4:car.speed_LF = speed-L_e7;       car.speed_LB = speed;  car.speed_RB = speed;    
					 car.speed_RF = speed+L_PLUSS7;  ;break;       //0100
		case 2:car.speed_LF = speed+L_PLUSS7;  car.speed_LB = speed;  car.speed_RB = speed;  
					 car.speed_RF = speed-L_e7;  ;break;       //0010
		case 12:car.speed_LF =speed-L_e8; car.speed_LB = speed;  car.speed_RB = speed;  
					 car.speed_RF = speed+L_PLUSS8; break;        //1100
		case 3:car.speed_LF = speed+L_PLUSS8; car.speed_LB = speed;  car.speed_RB = speed;  
					 car.speed_RF = speed-L_e8; break;       //0011
		case 8:car.speed_LF = speed-L_e9; car.speed_LB = speed;  car.speed_RB = speed;  
					 car.speed_RF = speed+L_PLUSS9; break;        //1000
		case 1:car.speed_LF = speed+L_PLUSS9; car.speed_LB = speed;  car.speed_RB = speed;  
					 car.speed_RF = speed-L_e9; break;       //0001

		default:car.speed_LF = speed;car.speed_RF = speed;	 car.speed_LB = speed;  car.speed_RB = speed;      break;
	}
}
```

---

### 对齐函数

```cpp
void alignmentB(float speed)//两点对一条线，一点碰到线马上停
```

## 库文件说明

```cpp
#include "catch.h"//舵机库函数
#include "line_track.h"//循迹运动库函数
```

## 主任务函数文件：

```cpp
maintask2.c
```
